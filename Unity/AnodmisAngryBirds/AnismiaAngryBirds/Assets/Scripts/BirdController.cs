﻿using System.Collections;
using UnityEngine;

public class BirdController : MonoBehaviour
{

    public Rigidbody2D rigidBody;
    public bool isActiveBird = false;
    bool hasHitFloor = false;

    public IEnumerator LookAtRightDirection()
    {
        Vector3 deltaPos = transform.position;
        yield return new WaitForEndOfFrame();
        while (!hasHitFloor)
        {
            transform.right = transform.position - deltaPos;
            deltaPos = transform.position;
            yield return new WaitForEndOfFrame();
        }

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        hasHitFloor = true;
    }
}
