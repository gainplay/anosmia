﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowBird : MonoBehaviour
{

    public Transform upperBoundGO, lowerBoundGO, LeftBoundGO, rightBoundGO;
    float lowerBound => lowerBoundGO.position.y;
    float upperrBound => upperBoundGO.position.y;
    float leftBound => LeftBoundGO.position.x;
    float rightBound => rightBoundGO.position.x;

    public GameObject birdGO;
    public SpriteRenderer bird;
    float lowerBirdBound => bird.bounds.center.y - bird.bounds.extents.y;
    float upperBirdBound => bird.bounds.center.y + bird.bounds.extents.y;
    float LeftBirdBound => bird.bounds.center.x - bird.bounds.extents.x;
    float rightBirdBound => bird.bounds.center.x + bird.bounds.extents.x;
    Vector3 birdLastFramePos;
    Vector3 birdCurrentPos => birdGO.transform.position;
    Vector3 birdDeltaPos => birdCurrentPos - birdLastFramePos;

    private void Awake()
    {
        birdLastFramePos = birdGO.transform.position;
    }

    private void Update()
    {
        if (lowerBirdBound < lowerBound || upperBirdBound > upperrBound)
        {
            transform.Translate(new Vector3(0, birdDeltaPos.y));
        }

        if (rightBirdBound > rightBound || LeftBirdBound < leftBound)
        {
            transform.Translate(new Vector3(birdDeltaPos.x, 0));
        }

        birdLastFramePos = birdGO.transform.position;

    }
}
