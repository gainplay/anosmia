﻿using UnityEngine;
using UnityEngine.UI;

public class Catapult : MonoBehaviour
{
    public BirdController currenntBird;
    public Rigidbody2D currentbirdRigidBody { get { return currenntBird.rigidBody; } }
    public LineRenderer frontLine, backLine;
    Vector3 birdStartPos;
    Vector3 touchStartPos;
    Vector3 touchEndPos;

    public Transform targetToPassToReleaseStrngs;

    public float tMaxPower;

    public Text debugstart, debugend;

    int firstTouchID = -1;
    private bool hasFired;

    private void Awake()
    {
        Debug.Log(Screen.width);
        currentbirdRigidBody.gravityScale = 0;
        birdStartPos = currentbirdRigidBody.position;
        frontLine.SetPosition(0, frontLine.transform.position);
        backLine.SetPosition(0, backLine.transform.position);
    }

    void Update()
    {
        if (currenntBird == null)
        {
            return;
        }

        if (!hasFired)
        {

            SetLinesToBird();
        }
        else if (currentbirdRigidBody.transform.position.x < targetToPassToReleaseStrngs.position.x)
        {
            SetLinesToBird();
        }
        else
        {
            currenntBird = null;
        }

        if (firstTouchID == -1 && Input.touches.Length > 0)
        {
            firstTouchID = Input.touches[0].fingerId;
            touchStartPos = Input.touches[0].position;
        }
        else if (firstTouchID != -1)
        {
            if (!TouchHelper.TouchesHasFingerID(firstTouchID))
            {
                Shoot();
                ResetTouchData();
            }
            else
            {
                Touch tCurrentTouch = TouchHelper.GetTouchByFingerId(firstTouchID);
                touchEndPos = tCurrentTouch.position;

                if (tCurrentTouch.phase == TouchPhase.Ended)
                {
                    Shoot();
                    ResetTouchData();
                }
                else
                {
                    currentbirdRigidBody.position = birdStartPos - new Vector3((touchStartPos.x - touchEndPos.x)/Screen.width, (touchStartPos.y - touchEndPos.y) / Screen.height)*3;
                    currentbirdRigidBody.transform.right = targetToPassToReleaseStrngs.position - currentbirdRigidBody.transform.position;
                }
            }
        }
    }

    void ResetTouchData()
    {
        firstTouchID = -1;
        touchStartPos.Set(0,0,0);
        touchEndPos.Set(0,0,0);
    }

    void Shoot()
    {
        hasFired = true;
        currentbirdRigidBody.gravityScale = 1;
        Vector3 tCurrentForce = (touchStartPos - touchEndPos);
        currentbirdRigidBody.velocity = Vector2.zero;
        currentbirdRigidBody.AddForce(tCurrentForce.magnitude > tMaxPower ? tCurrentForce.normalized * tMaxPower : tCurrentForce);
        currenntBird.StartCoroutine(currenntBird.LookAtRightDirection());
    }

    void SetLinesToBird()
    {
        frontLine.SetPosition(1, currentbirdRigidBody.transform.position);
        backLine.SetPosition(1, currentbirdRigidBody.transform.position);
    }
}
