﻿using UnityEngine;

public class CollisionParticles : MonoBehaviour
{
    public GameObject particles;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.GetComponent<BirdController>() != null)
        {
            Debug.Log($"Collision with name = {collision.collider.name}");
            particles.SetActive(true);
        }
    }
}
