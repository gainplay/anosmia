﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TouchHelper
{
    public static bool TouchesHasFingerID(int fingerId)
    {
        foreach (Touch touch in Input.touches)
        {
            if (touch.fingerId == fingerId)
            {
                return true;
            }
        }
        return false;
    }

    public static Touch GetTouchByFingerId(int fingerId)
    {
        foreach (Touch touch in Input.touches)
        {
            if (touch.fingerId == fingerId)
            {
                return touch;
            }
        }
        throw new NullReferenceException($"Touch with fingerId {fingerId} not found");
    }
}
