﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeTextThroughSlider : MonoBehaviour
{
    [SerializeField]
    string suffix;
    [SerializeField]
    UnityEngine.UI.Text textToChange;

    public void ChangeText(float sliderValue)
    {
        textToChange.text = Mathf.RoundToInt(sliderValue).ToString() + suffix;
    }

}
