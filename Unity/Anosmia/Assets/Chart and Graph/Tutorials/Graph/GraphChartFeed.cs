﻿using UnityEngine;
using ChartAndGraph;
using System;

public class GraphChartFeed : MonoBehaviour
{
   void Start()
   {
       GraphChartBase graph = GetComponent<GraphChartBase>();
       if (graph != null)
       {
           graph.Data.HorizontalViewOrigin = ChartDateUtility.DateToValue(DateTime.Today.AddDays(-7));
           graph.HorizontalValueToStringMap[0.0] = "Zero"; // example of how to set custom axis strings
           graph.DataSource.StartBatch();
           graph.DataSource.ClearCategory("Player 1");
           graph.DataSource.ClearCategory("Player 2");
           for (int i = 0; i < 30; i++)
           {
               //fromTheIncoming datetime of the first training of the day(player 2) and the last training of the day(player 1)
               //x = incoming ChartDateUtility.DateToValue(datetime.Date)
               //y = ChartDateUtility.TimeSpanToValue(datetime.TimeOfDay)
               graph.DataSource.AddPointToCategory("Player 1", ChartDateUtility.DateToValue(DateTime.Today.AddDays(i - 15)), i == 15 ? ChartDateUtility.TimeSpanToValue(DateTime.Now.TimeOfDay) : ChartDateUtility.TimeSpanToValue(TimeSpan.FromHours(17 + i % 5)));
               graph.DataSource.AddPointToCategory("Player 2", ChartDateUtility.DateToValue(DateTime.Today.AddDays(i - 15)), ChartDateUtility.TimeSpanToValue(TimeSpan.FromHours(i % 12 + 5)));
           }
           graph.DataSource.EndBatch();
       }
   }
}