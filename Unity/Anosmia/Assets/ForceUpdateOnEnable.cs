﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ForceUpdateOnEnable : MonoBehaviour
{
    RectTransform thisRecttransform;
    public void ForceUpdate()
    {
        if (thisRecttransform == null)
        {
            thisRecttransform = GetComponent<RectTransform>();
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(thisRecttransform);
    }
}
