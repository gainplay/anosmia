﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuToggleCheat : MonoBehaviour
{
    [SerializeField]
    Toggle _toggle;
    private void OnEnable()
    {
        StartCoroutine(SetToggleAtTndOfFrame());
    }

    IEnumerator SetToggleAtTndOfFrame()
    {
        yield return new WaitForEndOfFrame();
        _toggle.isOn = true;
    }
}
