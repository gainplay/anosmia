﻿using NotificationSamples;
#if UNITY_ANDROID
using NotificationSamples.Android;
using Unity.Notifications.Android;
#elif UNITY_IOS
using NotificationSamples.iOS;
using Unity.Notifications.iOS;
#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.Android;

public class MyNotificationManager : MonoBehaviour
{
    // On iOS, this represents the notification's Category Identifier, and is optional
    // On Android, this represents the notification's channel, and is required (at least one).
    // Channels defined as global constants so can be referred to from GameController.cs script when setting/sending notification
    public const string morningId = "morning training";
    public const string eveningId = "eventing training";
    public const string diaryId = "diary";
    public const string defaultId = "default";

    public string morningTitle;
    public string morningBody;
    public string eveningTitle;
    public string eveningBody;
    public string diaryTitle;
    public string diaryBody;

    [SerializeField]
    protected GameNotificationsManager manager;




    //is an ienumerator for the yield return in the #if ios
#if UNITY_IOS
    private IEnumerator Start()
    {
        yield return RequestAuthorization();
#endif

#if UNITY_ANDROID
    private void Start()
    {
        if (!Permission.HasUserAuthorizedPermission("android.permission.SCHEDULE_EXACT_ALARM"))
        {
            Permission.RequestUserPermission("android.permission.SCHEDULE_EXACT_ALARM");
        }
        // Set up channels (mostly for Android)
        // You need to have at least one of these
        var c0 = new GameNotificationChannel(defaultId, "Default Game Channel", "Generic notifications");
        var c1 = new GameNotificationChannel(morningId, "morning notifications", "do your morning training");
        var c2 = new GameNotificationChannel(eveningId, "evening notifications", "do your evening training");
        var c3 = new GameNotificationChannel(diaryId, "diary notifications", "Diary reminders");
        manager.Initialize(c0, c1, c2, c3);

        CancelAllNotifications();
#endif
        if (SettingsHolder.birthYear == 0)
        {
            SendMorningNotification("06:00");
            SendEveningNotification("19:00");
        }
        else if (NotificationPlayerPrefHelper.HasEnabledKey ? NotificationPlayerPrefHelper.NotificationsEnabled : false)
        {
            ScheduleNotifications();
        }



    }

    public void CancelAllNotifications()
    {
#if UNITY_ANDROID

        manager.CancelAllNotifications();

#elif UNITY_IOS
        iOSNotificationCenter.RemoveScheduledNotification(morningId);
        iOSNotificationCenter.RemoveScheduledNotification(eveningId);
        iOSNotificationCenter.RemoveScheduledNotification(diaryId);
#endif
    }

    public void ScheduleNotifications()
    {


        SendMorningNotification(NotificationPlayerPrefHelper.HasMorningKey ? NotificationPlayerPrefHelper.MorningTime : "06:00");
        SendEveningNotification(NotificationPlayerPrefHelper.HasEveningKey ? NotificationPlayerPrefHelper.EveningTime : "19:00");
        SendDiaryNotification();
    }

    private void OnApplicationFocus(bool focus)
    {
        if (focus)
        {
            manager.DismissAllNotifications();
        }
    }

    private void SendDiaryNotification()
    {
        DateTime tSendDate = DateTime.Now;
        if (tSendDate.DayOfWeek == DayOfWeek.Saturday && tSendDate.Hour < 12)
        {
            tSendDate = tSendDate.Date.AddHours(12);
        }
        else
        {
            while (tSendDate.DayOfWeek != DayOfWeek.Saturday)
            {
                tSendDate = tSendDate.AddDays(1);
            }
        }
#if UNITY_ANDROID
        SendAndroidNotification(diaryTitle, diaryBody, tSendDate, channelId: diaryId, reschedule: true, smallIcon: "icon_small", largeIcon: "icon");
#elif UNITY_IOS
        TimeSpan offset = tSendDate - DateTime.Now;
        SendIOSNotification(offset.Days, offset.Hours, offset.Minutes, diaryId, diaryTitle, diaryBody);
#endif
    }

    public void SendMorningNotification(string timeString)
    {
        string[] tSplitTimes = timeString.Split(':');
#if UNITY_ANDROID
        DateTime tNotificationTime = DateTime.ParseExact(
                                                            DateTime.Now.ToString("yyyy-MM-dd"),
                                                            "yyyy-MM-dd",
                                                            CultureInfo.InvariantCulture
                                                        )
                                                        +
                                                        new TimeSpan(
                                                        int.Parse(tSplitTimes[0]),
                                                        int.Parse(tSplitTimes[1]),
                                                        0
                                                        );
        List<int?> tIds = new List<int?>();
        foreach (var item in manager.PendingNotifications)
        {
            if (item.Notification.Title == morningTitle)
            {
                tIds.Add(item.Notification.Id);
            }
        }
        foreach (var item in tIds)
        {
            if (item != null)
            {
                manager.CancelNotification((int)item);
            }
        }

        if (DateTime.Now < tNotificationTime)
        {
            SendAndroidNotification(morningTitle, morningBody, tNotificationTime, channelId: morningId, reschedule: true, smallIcon: "icon_small", largeIcon: "icon");
        }
        for (int i = 0; i < 30; i++)
        {
            SendAndroidNotification(morningTitle, morningBody, tNotificationTime + TimeSpan.FromDays(1), channelId: morningId, reschedule: true, smallIcon: "icon_small", largeIcon: "icon");
            tNotificationTime += TimeSpan.FromDays(1);
        }
#elif UNITY_IOS
        SendIOSNotification(int.Parse(tSplitTimes[0]), int.Parse(tSplitTimes[1]),morningId,morningTitle,morningBody);

#endif
    }

    public void SendEveningNotification(string timeString)
    {
        string[] tSplitTimes = timeString.Split(':');
#if UNITY_ANDROID
        DateTime tNotificationTime = DateTime.ParseExact(
                                                                            DateTime.Now.ToString("yyyy-MM-dd"),
                                                                            "yyyy-MM-dd",
                                                                            CultureInfo.InvariantCulture
                                                                        )
                                                                        +
                                                                            new TimeSpan(
                                                                                            int.Parse(tSplitTimes[0]),
                                                                                            int.Parse(tSplitTimes[1]),
                                                                                            0
                                                                                        );
        List<int?> tIds = new List<int?>();
        foreach (var item in manager.PendingNotifications)
        {
            if (item.Notification.Title == eveningTitle)
            {
                tIds.Add(item.Notification.Id);
            }
        }
        foreach (var item in tIds)
        {
            if (item != null)
            {
                manager.CancelNotification((int)item);
            }
        }

        if (DateTime.Now < tNotificationTime)
        {
            SendAndroidNotification(eveningTitle, eveningBody, tNotificationTime, channelId: morningId, reschedule: true, smallIcon: "icon_small", largeIcon: "icon");
        }
        for (int i = 0; i < 30; i++)
        {
            SendAndroidNotification(eveningTitle, eveningBody, tNotificationTime + TimeSpan.FromDays(1), channelId: morningId, reschedule: true, smallIcon: "icon_small", largeIcon: "icon");
            tNotificationTime += TimeSpan.FromDays(1);
        }

#elif UNITY_IOS
        SendIOSNotification(int.Parse(tSplitTimes[0]), int.Parse(tSplitTimes[1]), eveningId, eveningTitle, eveningBody);

#endif

    }

    /// <summary>
    /// Queue a notification with the given parameters.
    /// </summary>
    /// <param name="title">The title for the notification.</param>
    /// <param name="body">The body text for the notification.</param>
    /// <param name="deliveryTime">The time to deliver the notification.</param>
    /// <param name="badgeNumber">The optional badge number to display on the application icon.</param>
    /// <param name="reschedule">
    /// Whether to reschedule the notification if foregrounding and the notification hasn't yet been shown.
    /// </param>
    /// <param name="channelId">Channel ID to use. If this is null/empty then it will use the default ID. For Android
    /// the channel must be registered in <see cref="GameNotificationsManager.Initialize"/>.</param>
    /// <param name="smallIcon">Notification small icon.</param>
    /// <param name="largeIcon">Notification large icon.</param>
    public void SendAndroidNotification(string title, string body, DateTime deliveryTime, int? badgeNumber = null,
                                 bool reschedule = false, string channelId = null,
                                 string smallIcon = null, string largeIcon = null)
    {

        Debug.Log("pre request permission");
        if (!Permission.HasUserAuthorizedPermission("android.permission.SCHEDULE_EXACT_ALARM"))
        {
            Debug.Log("requesting permission");
            Permission.RequestUserPermission("android.permission.SCHEDULE_EXACT_ALARM");
        }
        else
        {
            Debug.Log("has push notification permission");

        }
        Debug.Log("post request permission");
        IGameNotification notification = manager.CreateNotification();

        if (notification == null)
        {
            return;
        }

        notification.Title = title;
        notification.Body = body;
        notification.Group = !string.IsNullOrEmpty(channelId) ? channelId : defaultId;
        notification.DeliveryTime = deliveryTime;
        notification.SmallIcon = smallIcon;
        notification.LargeIcon = largeIcon;
        if (badgeNumber != null)
        {
            notification.BadgeNumber = badgeNumber;
        }

        PendingNotification notificationToDisplay = manager.ScheduleNotification(notification);
        notificationToDisplay.Reschedule = reschedule;

    }



#if UNITY_IOS
    IEnumerator RequestAuthorization()
    {
        Debug.Log("IOS: start authorization");
        using (var req = new AuthorizationRequest(AuthorizationOption.Alert | AuthorizationOption.Badge, true))
        {
            while (!req.IsFinished)
            {
                yield return null;
            };

            string res = "\n RequestAuthorization: \n";
            res += "\n finished: " + req.IsFinished;
            res += "\n granted :  " + req.Granted;
            res += "\n error:  " + req.Error;
            res += "\n deviceToken:  " + req.DeviceToken;
            Debug.Log("IOS: " + res);
        }
        Debug.Log("IOS: schedule notification in 5 minutes");
    }

    void SendIOSNotification(int hours, int minutes, string identifier, string title, string body)
    {

        iOSNotificationCenter.RemoveScheduledNotification(identifier);

        var timeTrigger = new iOSNotificationCalendarTrigger()
        {
            Hour = hours,
            Minute = minutes,
            Repeats = true
        };

        var notification = new iOSNotification()
        {
            // You can optionally specify a custom identifier which can later be 
            // used to cancel the notification, if you don't set one, a unique 
            // string will be generated automatically.
            Identifier = identifier,
            Title = title,
            Body = body,
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            Trigger = timeTrigger,
        };

        iOSNotificationCenter.ScheduleNotification(notification);
        Debug.Log("IOS: scheduled the notification");

    }

    void SendIOSNotification(int day, int hours, int minutes, string identifier, string title, string body)
    {

        iOSNotificationCenter.RemoveScheduledNotification(identifier);

        var timeTrigger = new iOSNotificationTimeIntervalTrigger()
        {
            TimeInterval = new TimeSpan(day * 24 + hours, minutes, 0),
            Repeats = false
        };

        var notification = new iOSNotification()
        {
            // You can optionally specify a custom identifier which can later be 
            // used to cancel the notification, if you don't set one, a unique 
            // string will be generated automatically.
            Identifier = identifier,
            Title = title,
            Body = body,
            ShowInForeground = true,
            ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
            Trigger = timeTrigger,
        };

        iOSNotificationCenter.ScheduleNotification(notification);
        Debug.Log("IOS: scheduled the notification");

    }
#endif



}
