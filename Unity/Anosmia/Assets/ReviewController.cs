﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
#if UNITY_ANDROID
using GooglePlayGames.Android;
using Google.Play.Review;
#endif
public class ReviewController : MonoBehaviour
{
#if UNITY_ANDROID
    private ReviewManager reviewManager;
#endif
    IEnumerator reviewFlowRoutine;
    const string playerPrefsStartDateKey = "startdateforreview";
    System.DateTime bewtweenReviewsCooldown = new System.DateTime();

    string[] playerprefKeys = new string[7];
    private void Awake()
    {
#if UNITY_ANDROID
        reviewManager = new ReviewManager();
#endif
        OnApplicationPause(false);
    }
    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            if (PlayerPrefs.HasKey(playerPrefsStartDateKey))
            {
                Debug.Log($"datetime now = {System.DateTime.Now}, playerpref string = {PlayerPrefs.GetString(playerPrefsStartDateKey)}");
                if (System.DateTime.Now > System.DateTime.Parse(PlayerPrefs.GetString(playerPrefsStartDateKey)))
                {
                    StartReview();
                }
            }
            else
            {
                Debug.Log("no playerpref string");
                SetReviewCooldown();
            }
        }
    }

    void SetReviewCooldown()
    {
        PlayerPrefs.SetString(playerPrefsStartDateKey, System.DateTime.Now.AddDays(5).Date.ToString());
        Debug.Log(PlayerPrefs.GetString(playerPrefsStartDateKey));
    }
    public void SetReviewCooldown(int playerprefKey)
    {
        PlayerPrefs.SetString(playerPrefsStartDateKey, System.DateTime.Now.AddDays(5).ToString());
    }

    public void DeleteDebugKeys()
    {
        foreach (var item in playerprefKeys)
        {
            PlayerPrefs.DeleteKey(item);

        }
        PlayerPrefs.Save();
    }
    public void StartReview()
    {
        if (System.DateTime.Now < bewtweenReviewsCooldown)
        {
            return;
        }
        bewtweenReviewsCooldown = System.DateTime.Now.AddMinutes(1);
        SetReviewCooldown();
#if UNITY_ANDROID
        if (reviewFlowRoutine != null)
        {
            StopCoroutine(reviewFlowRoutine);

        }
        reviewFlowRoutine = GoogleReviewFlow();
        StartCoroutine(reviewFlowRoutine);
#elif UNITY_IOS
        Device.RequestStoreReview();
#endif
    }



#if UNITY_ANDROID
    IEnumerator GoogleReviewFlow()
    {

        var requestFlowOperation = reviewManager.RequestReviewFlow();
        yield return requestFlowOperation;
        if (requestFlowOperation.Error != ReviewErrorCode.NoError)
        {
            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }
        PlayReviewInfo playReviewInfo = requestFlowOperation.GetResult();



        var launchFlowOperation = reviewManager.LaunchReviewFlow(playReviewInfo);
        yield return launchFlowOperation;
        playReviewInfo = null; // Reset the object
        if (launchFlowOperation.Error != ReviewErrorCode.NoError)
        {
            // Log error. For example, using requestFlowOperation.Error.ToString().
            yield break;
        }
        // The flow has finished. The API does not indicate whether the user
        // reviewed or not, or even whether the review dialog was shown. Thus, no
        // matter the result, we continue our app flow.

    }
#endif
}
