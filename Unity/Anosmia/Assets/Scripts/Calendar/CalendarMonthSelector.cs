﻿using System.Collections;
using System.Collections.Generic;
using UI.Dates;
using UnityEngine;

public class CalendarMonthSelector : MonoBehaviour
{
    [SerializeField]
    DatePicker _datePicker;
    void Start()
    {
        _datePicker.ShowNextMonth();
        _datePicker.ShowPreviousMonth();
    }
}
