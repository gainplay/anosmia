﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class TestKalendar : MonoBehaviour
{
    public static Action Refresh;

    [SerializeField]
    Image apointmentImage;
    [SerializeField]
    Sprite apointmentSprite, noApointmentSprite;

    void Start()
    {
        Debug.Log("kalender vandaag"  + gameObject.name);
        ToggleImageIfNeeded();
        Refresh += ToggleImageIfNeeded;
    }

    private void OnDestroy()
    {
        Refresh -= ToggleImageIfNeeded;
    }

    public void ToggleImageIfNeeded()
    {
        apointmentImage.sprite = DiaryController.diaryItems.Find(c => c.date.Date == DateTime.ParseExact(gameObject.name, "yyyy-MM-dd", CultureInfo.InvariantCulture)) != null ? apointmentSprite : noApointmentSprite;
    }
}
