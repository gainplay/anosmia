﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DeleteUser : MonoBehaviour
{
    public void DeleteUserClicked()
    {
        StartCoroutine(DeleteUserRoutine());
    }

    private IEnumerator DeleteUserRoutine()
    {
        WWWForm tFrom = new WWWForm();
        tFrom.AddField("id", SocialController.GetUniqueID);
        UnityWebRequest webRequest = UnityWebRequest.Post(DatabaseHelper.baseUrl + "deleteuser.php",tFrom);
        yield return webRequest.SendWebRequest();
        if (!DatabaseHelper.HasError(webRequest) || webRequest.downloadHandler.text == "true")
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.Save();
            Application.Quit();
        }
    }
}
