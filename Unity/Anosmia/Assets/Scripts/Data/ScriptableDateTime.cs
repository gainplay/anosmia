﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Scriptable/NativeValue/DateTime")]
public class ScriptableDateTime : ScriptableObject
{
    [SerializeField]
    string _timeString;
    [SerializeField]
    string _format;
    public DateTime dateTime => DateTime.ParseExact(_timeString,_format, System.Globalization.CultureInfo.InvariantCulture);
}
