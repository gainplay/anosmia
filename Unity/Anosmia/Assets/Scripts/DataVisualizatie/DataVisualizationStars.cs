﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class DataVisualizationStars : MonoBehaviour
{
    [SerializeField]
    Toggle[] toggles;
    [SerializeField]
    GameObject[] starTexts;
    [SerializeField]
    GameObject starHolder;

    private void OnEnable()
    {
        if (DiaryController.diaryItems.Count == 0)
        {
            starHolder.SetActive(false);
            return;
        }
        int highestStar = (from item in DiaryController.diaryItems orderby item.smellToday descending select item).FirstOrDefault().smellToday;

        if (highestStar > 0)
        {
            starHolder.SetActive(true);
            starTexts[0].SetActive(false);
            for (int i = 0; i < toggles.Length; i++)
            {
                toggles[i].isOn = i <= highestStar;
                starTexts[i+1].SetActive(i == highestStar);
            }
        }
        else
        {
            starHolder.SetActive(false);
        }
    }



}
