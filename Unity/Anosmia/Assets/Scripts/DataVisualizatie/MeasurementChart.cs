﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using System.Linq;
using System;

public class MeasurementChart : MonoBehaviour
{
    [SerializeField]
    GraphChart _chart;

    private void OnEnable()
    {
        if (DiaryController.diaryItems.Count > 0)
        {
            _chart.Data.StartBatch();
            _chart.DataSource.ClearCategory("Player 1");
            foreach (var item in DiaryController.diaryItems)
            {
                _chart.DataSource.AddPointToCategory("Player 1", ChartDateUtility.DateToValue(item.date.Date), item.subProgress);
            }
            DateTime tDateTime = (from r in DiaryController.diaryItems orderby r.date descending select r.date).FirstOrDefault();
            Debug.Log($"tDateTime = {tDateTime}");
            _chart.HorizontalScrolling = ChartDateUtility.DateToValue(DiaryController.diaryItems.OrderByDescending(i => i.date).First().date.Date.AddDays(-3));
            _chart.DataSource.EndBatch(); 
        }
    }
}
