﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChartAndGraph;
using System.Linq;
public class MinigamePieChart : MonoBehaviour
{
    [SerializeField]
    CanvasPieChart _chart;
    private void OnEnable()
    {
        if (MinigameResultController.minigameResults.Count >0)
        {
            List<pieEntry> counts = (from r in MinigameResultController.minigameResults
                                     group r by r.minigame into g
                                     where g.Key != -1
                                     select new pieEntry() { minigame = g.Key, count = g.Count() }).ToList();
            foreach (var item in counts)
            {
                Debug.Log($"category name{_chart.DataSource.GetCategoryName(item.minigame)}, index = {item.minigame}, count = {item.count}");
                _chart.DataSource.SetValue(_chart.DataSource.GetCategoryName(item.minigame), item.count);
            }

        }
    }


    struct pieEntry { public int minigame, count; }
}
