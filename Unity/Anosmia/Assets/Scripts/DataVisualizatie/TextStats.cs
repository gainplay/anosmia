﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using System.Linq;

public class TextStats : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI streak;
    [SerializeField]
    TextMeshProUGUI minigamesPlayed;
    [SerializeField]
    TextMeshProUGUI diaryEntries;

    private void OnEnable()
    {
        List<DateTime> tStreakDates = (from item in MinigameResultController.minigameResults group item by item.trainingTime.Date into g select g.Key).OrderByDescending(t => t.Date).ToList();
        int tStreak = 0;
        DateTime tLastDate = DateTime.Now.Date;
        for (int i = 0; i < tStreakDates.Count; i++)
        {
            if (i == 0 && tStreakDates[i].Date == tLastDate.Date)
            {
                continue;
            }
            if (tStreakDates[i].Date == tLastDate.AddDays(-1).Date)
            {
                tStreak++;
                tLastDate = tStreakDates[i].Date;
            }
        }
        streak.text = tStreak.ToString();

        minigamesPlayed.text = (from item in MinigameResultController.minigameResults where item.minigame != -1 select item).ToList().Count.ToString();

        diaryEntries.text = (from item in DiaryController.diaryItems where !string.IsNullOrEmpty(item.text) select item).ToList().Count.ToString();
    }
}
