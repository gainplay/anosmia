﻿using ChartAndGraph;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;


public class TrainTimeChart : MonoBehaviour
{
    [SerializeField]
    GraphChart _chart;

    private void OnEnable()
    {
        if (MinigameResultController.minigameResults.Count > 0)
        {
            _chart.Data.StartBatch();
            _chart.DataSource.ClearCategory("Player 1");
            _chart.DataSource.ClearCategory("Player 2");

            List<TrainTimeChartGroup> tResults = (from result in MinigameResultController.minigameResults group result by result.trainingTime.Date into g select new TrainTimeChartGroup(g.Key, g.ToList())).ToList();
            foreach (var item in tResults)
            {
                List<MinigameResultItem> items = item.group.OrderByDescending(i => i.trainingTime).ToList();
                MinigameResultItem itemHigh, ItemLow;
                itemHigh = items[0];
                ItemLow = items[items.Count - 1];
                _chart.DataSource.AddPointToCategory("Player 1", ChartDateUtility.DateToValue(itemHigh.trainingTime.Date), ChartDateUtility.TimeSpanToValue(itemHigh.trainingTime - itemHigh.trainingTime.Date));
                _chart.DataSource.AddPointToCategory("Player 2", ChartDateUtility.DateToValue(ItemLow.trainingTime.Date), ChartDateUtility.TimeSpanToValue(ItemLow.trainingTime - ItemLow.trainingTime.Date));
            }
            Debug.Log($"pre horizontalscrolling = {_chart.HorizontalScrolling}");

            _chart.HorizontalScrolling = ChartDateUtility.DateToValue(MinigameResultController.minigameResults.OrderByDescending(i => i.trainingTime).First().trainingTime.Date.AddDays(-3));
            Debug.Log($"post horizontalscrolling = {_chart.HorizontalScrolling}");
            _chart.DataSource.EndBatch(); 
        }
    }


    struct TrainTimeChartGroup
    {
        public DateTime date;
        public List<MinigameResultItem> group;

    public TrainTimeChartGroup(DateTime date, List<MinigameResultItem> group)
    {
        this.date = date;
        this.group = group;
    }
}
}
