﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class CheckIfUniqueIdExistsInDatabase
{
    public static IEnumerator DoesUniqueIdExistsInDatabase(string uniqueId, Action<bool> callback)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "doesUserExist.php");
        tRequest.SetRequestHeader("UNIQUEID", uniqueId);
        yield return tRequest.SendWebRequest();
        if (!tRequest.HasError())
        {
            if (bool.TryParse(tRequest.downloadHandler.text, out bool tResult))
            {
                callback(tResult);
            }
            else
            {
                callback(false);
            }
        }
    }
}
