﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DatabaseDebug : MonoBehaviour
{

    public string testId;
    // Start is called before the first frame update
    IEnumerator Start()
    {
       yield return CheckIfUniqueIdExistsInDatabase.DoesUniqueIdExistsInDatabase(testId, ResultHandler);
    }

    private void ResultHandler(bool result)
    {
        Debug.Log($"username exists = {result}");
    }
}
