﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class DatabaseHelper
{
    public static string baseUrl = "https://reuktraining.nl/gamescripts/";

    public static bool HasError(this UnityWebRequest request)
    {
        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log(request.error);
            return true;
        }
        return false;
    }

}
