﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static  class GetInitialData
{
    public static IEnumerator GetInitialDataFromDB(string uniqueId, Action<bool> hasDataCallback)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "getInitialData.php");
        tRequest.SetRequestHeader("UNIQUEID", uniqueId);
        yield return tRequest.SendWebRequest();
        if (!tRequest.HasError())
        {
            Debug.Log(tRequest.downloadHandler.text);
            SettingsHolder.dataStruct = JsonUtility.FromJson<SettingsHolderStruct>(tRequest.downloadHandler.text);
            hasDataCallback(SettingsHolder.birthYear != 0);
            Debug.Log($"Test Json = {JsonUtility.ToJson(SettingsHolder.dataStruct)}");
        }
    }
}
