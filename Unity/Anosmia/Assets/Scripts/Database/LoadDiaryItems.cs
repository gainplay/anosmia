﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class LoadDiaryItems : MonoBehaviour
{
    public static IEnumerator LoadDiaryItemsFromDatabase(string id, Action<List<DiaryItem>> diaryItemsCallback)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "loadDiaryItems.php");
        tRequest.SetRequestHeader("UNIQUEID", id);
        yield return tRequest.SendWebRequest();
        List<DiaryJsonBridge> tBridgeList = new List<DiaryJsonBridge>();
        tBridgeList.FromJson(tRequest.downloadHandler.text);
        List<DiaryItem> tDiaryList = new List<DiaryItem>();
        foreach (var item in tBridgeList)
        {
            tDiaryList.Add(new DiaryItem(item));
        }
        diaryItemsCallback(tDiaryList);
        Debug.Log(tRequest.downloadHandler.text);
    }
}
