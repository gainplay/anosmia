﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class LoadMinigameResults
{
    public static IEnumerator LoadMinigameResultsFromDatabase(string id, Action<List<MinigameResultItem>> diaryItemsCallback)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "loadMinigameResults.php");
        tRequest.SetRequestHeader("UNIQUEID", id);
        yield return tRequest.SendWebRequest();
        List<MinigameResultJsonBridge> tBridgeList = new List<MinigameResultJsonBridge>();
        tBridgeList.FromJson(tRequest.downloadHandler.text);
        List<MinigameResultItem> tMinigameResults = new List<MinigameResultItem>();
        foreach (var item in tBridgeList)
        {
            tMinigameResults.Add(new MinigameResultItem(item));
        }
        diaryItemsCallback(tMinigameResults);
        Debug.Log(tRequest.downloadHandler.text);
    }
}
