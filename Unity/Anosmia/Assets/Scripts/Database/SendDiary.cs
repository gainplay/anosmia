﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class SendDiary
{
    public static IEnumerator SendDiaryResultToDB(string id, DateTime date, string textEntry, int subProgression, int smellToday, Action<bool> successCallback)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "sendDiary.php");
        tRequest.SetRequestHeader("UNIQUEID", id);
        tRequest.SetRequestHeader("DATE", date.ToString("yyyy-MM-dd"));
        tRequest.SetRequestHeader("TEXTENTRY", textEntry);
        tRequest.SetRequestHeader("SUBPROGRESSION", subProgression.ToString());
        tRequest.SetRequestHeader("SMELLTODAY", smellToday.ToString());
        yield return tRequest.SendWebRequest();
        if (tRequest.HasError())
        {
            successCallback(false);

        }
        else
        {
            successCallback(true);
        }
        Debug.Log(tRequest.downloadHandler.text);
    }
}
