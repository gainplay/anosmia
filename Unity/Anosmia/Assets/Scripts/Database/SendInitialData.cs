﻿using System;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine;

public static class SendInitialData
{
    public static IEnumerator SendInitialDataToDatabase(SendInitialDataStruct dataToSend, Action<bool> successCallback)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "sendInitialData.php");
        tRequest.SetRequestHeader("INITIALDATA", UnityEngine.JsonUtility.ToJson(dataToSend));
        yield return tRequest.SendWebRequest();
        if (!tRequest.HasError())
        {
            bool.TryParse(tRequest.downloadHandler.text, out bool result);
            Debug.Log(tRequest.downloadHandler.text);
            successCallback(result);
        }
        else
        {
            Debug.Log(tRequest.error);
            successCallback(false);
        }
    }
}

[System.Serializable]
public struct SendInitialDataStruct
{
    public string uniqueId;
    public bool permission;
    public int birthYear;
    public int gender;
    public string causeOfAnosmia;
    public bool diagnosedByDoctor; 
    public string anosmiaFrom;
    public int anosmiaTrainingExperience;
    public int smellAbility;

    public SendInitialDataStruct(string uniqueId, bool permission, int birthYear, int gender, string causeOfAnosmia, bool diagnosedByDoctor, string anosmiaFrom, int anosmiaTrainingExperience, int smellAbility)
    {
        this.uniqueId = uniqueId;
        this.permission = permission;
        this.birthYear = birthYear;
        this.gender = gender;
        this.causeOfAnosmia = causeOfAnosmia;
        this.diagnosedByDoctor = diagnosedByDoctor;
        this.anosmiaFrom = anosmiaFrom;
        this.anosmiaTrainingExperience = anosmiaTrainingExperience;
        this.smellAbility = smellAbility;
    }
}