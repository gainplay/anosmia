﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SendLogin : MonoBehaviour
{
    IEnumerator Start()
    {
        yield return new WaitForSeconds(1);
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "sendLogin.php");
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
    }
}
