﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public static class SendMinigameResult
{
    public static IEnumerator SendMinigameResultToDB(int smell, int minigame, Action<bool> successCallback)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "sendMinigameResult.php");
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        tRequest.SetRequestHeader("SMELL", smell.ToString());
        tRequest.SetRequestHeader("SMELLSET", SettingsHolder.smellSetSelected.ToString());
        tRequest.SetRequestHeader("MINIGAME", minigame.ToString());
        yield return tRequest.SendWebRequest();
        if (tRequest.HasError())
        {
            successCallback(false);

        }
        else if(bool.TryParse(tRequest.downloadHandler.text, out bool tResult))
        {
            successCallback(tResult);
        }
        else
        {
            successCallback(false);
        }
        Debug.Log(tRequest.downloadHandler.text);
    }
}
