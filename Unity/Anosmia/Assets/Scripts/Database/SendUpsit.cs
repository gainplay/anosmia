﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static  class SendUpsit
{
    public static IEnumerator SendUpsitToDatabase(int score, int time, string id, Action<bool> errorCallback)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "sendUpsit.php");
        tRequest.SetRequestHeader("UNIQUEID", id);
        tRequest.SetRequestHeader("UPSITSCORE", score.ToString());
        tRequest.SetRequestHeader("UPSITTIME", time.ToString());
        yield return tRequest.SendWebRequest();
        errorCallback(tRequest.HasError());
        Debug.Log(tRequest.downloadHandler.text);
    }
}
