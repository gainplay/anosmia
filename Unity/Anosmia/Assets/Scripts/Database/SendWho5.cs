﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public static class SendWho5
{
    public static IEnumerator SendWho5ToDatabase(int score, int time, string id, Action<bool> errorCallback)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "sendWho5.php");
        tRequest.SetRequestHeader("UNIQUEID", id);
        tRequest.SetRequestHeader("WHOSCORE", score.ToString());
        tRequest.SetRequestHeader("WHOTIME", time.ToString());
        yield return tRequest.SendWebRequest();
        errorCallback(tRequest.HasError());
        Debug.Log(tRequest.downloadHandler.text);
    }
}
