﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Networking;

public class DebugAutenticate : MonoBehaviour
{
    
    [SerializeField]
    TextMeshProUGUI _textfield;
    // Start is called before the first frame update
    private IEnumerator Start()
    {

        UnityWebRequest tRequest = UnityWebRequest.Get("http://192.168.1.106/Noseme/Test.php");
        yield return tRequest.SendWebRequest();
        if (string.IsNullOrEmpty(tRequest.error))
        {
        _textfield.text = tRequest.downloadHandler.text;

        }
        else
        {
            _textfield.text = tRequest.error;
        }

        while (!SocialController.TryGetUniqueID(out string tID))
        {
            yield return 0;
        }
        _textfield.text = SocialController.GetUniqueID;


        
    }


    public void ShowIDAndUser()
    {
        _textfield.text = $"<id>{Social.localUser.id}</id>\n<username>{Social.localUser.userName}</username>";
    }

   

    public void Showleaderboard() 
    {
        SocialController.ShowLeaderBoard();
    }


}
