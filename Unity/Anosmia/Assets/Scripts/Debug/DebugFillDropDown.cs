﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class DebugFillDropDown : SerializedMonoBehaviour
{
    public int fillFrom;
    public int fillTo;
    public int numOfNumbers =1;
    private string from => fillFrom.ToString($"D{numOfNumbers}");
    private string to => fillTo.ToString($"D{numOfNumbers}");
    [Button("Fill dropdown from fillFrom to fillTo")]
    public void FillDropDown()
    {
        List<Dropdown.OptionData> tOptions = new List<Dropdown.OptionData>();
        for (int i = fillFrom; i <= fillTo; i++)
        {
            tOptions.Add(new Dropdown.OptionData((i).ToString($"D{numOfNumbers}")));
        }
        GetComponent<Dropdown>().options = tOptions;
    }
}
