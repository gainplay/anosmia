﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleOrientation : MonoBehaviour
{
    bool _isPortrait = true;

    public void ToggleTheOrientation()
    {
        _isPortrait = !_isPortrait;
        Screen.autorotateToPortrait = _isPortrait;
        Screen.autorotateToPortraitUpsideDown = _isPortrait;
        Screen.autorotateToLandscapeLeft = !_isPortrait;
        Screen.autorotateToLandscapeRight = !_isPortrait;

        Screen.orientation = _isPortrait ? ScreenOrientation.Portrait : ScreenOrientation.LandscapeLeft;
        Screen.orientation = ScreenOrientation.AutoRotation;
    }
}
