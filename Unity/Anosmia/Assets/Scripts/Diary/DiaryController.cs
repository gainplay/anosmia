﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiaryController : MonoBehaviour
{
    public static List<DiaryItem> diaryItems = new List<DiaryItem>();

    DiaryItem currentItem;

    public static IEnumerator LoadDiaryItemsRoutine()
    {
        diaryItems.Clear();
        yield return LoadDiaryItems.LoadDiaryItemsFromDatabase(SocialController.GetUniqueID, l => diaryItems = l);
        diaryItems.Sort((x,y) => { return x.date.CompareTo(y.date); });
    }

}
