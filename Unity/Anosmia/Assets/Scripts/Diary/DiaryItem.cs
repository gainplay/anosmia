﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DiaryItem
{
    public DateTime date;
    public string text;
    public int subProgress;
    public int smellToday;

    public DiaryItem(DiaryJsonBridge jsonBridge)
    {
        this.date = DateTime.ParseExact(jsonBridge.date,"yyyy-MM-dd", null);
        this.text = jsonBridge.text;
        this.subProgress = jsonBridge.subProgress;
        this.smellToday = jsonBridge.smellToday;
    }

    public DiaryItem(DateTime date, string text, int subProgress, int smellToday)
    {
        this.date = date;
        this.text = text;
        this.subProgress = subProgress;
        this.smellToday = smellToday;
    }

    public string ToJson()
    {
        return JsonUtility.ToJson(GetDiaryJsonBridge);
    }

    public DiaryJsonBridge GetDiaryJsonBridge { get { return new DiaryJsonBridge(this); } }
}

[Serializable]
public struct DiaryJsonBridge
{
    public string date;
    public string text;
    public int subProgress;
    public int smellToday;

    public DiaryJsonBridge(DiaryItem diaryItem)
    {
        this.date = diaryItem.date.ToString("yyyy-MM-dd");
        this.text = diaryItem.text;
        this.subProgress = diaryItem.subProgress;
        this.smellToday = diaryItem.smellToday;
    }

    public DiaryJsonBridge(string date, string text, int subProgress, int smellToday)
    {
        this.date = date;
        this.text = text;
        this.subProgress = subProgress;
        this.smellToday = smellToday;
    }

    public DiaryItem GetDiaryItem{get{ return new DiaryItem(this); }}
}
