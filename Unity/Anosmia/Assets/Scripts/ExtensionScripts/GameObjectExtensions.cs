﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions
{

    /// <summary>
    /// sets this gameobject and all parents to the target state
    /// Upper gameobject gets SetActive called first
    /// </summary>
    /// <param name="gameObject">the gameobject to look up from</param>
    /// <param name="target">the setactive target</param>
    public static void SetThisAndAllParentsActive(this GameObject gameObject, bool target)
    {
        gameObject.GetParent()?.SetThisAndAllParentsActive(target);
        gameObject.SetActive(target);
    }

    /// <summary>
    /// returns the parent gameobject
    /// returns null if there is no parent
    /// </summary>
    /// <param name="gameObject">the gameobject to look for the parent for</param>
    /// <returns>the parent  if succesfull null if there is no parent</returns>
    public static GameObject GetParent(this GameObject gameObject)
    {
        return gameObject.transform.parent?.gameObject;
    }


    public static Vector3 GetPosition(this GameObject gameObject)
    {
        return gameObject.transform.position;
    }

    public static void SetPosition(this GameObject gameobject, Vector3 newPos)
    {
        gameobject.transform.position = newPos;
    }
}
