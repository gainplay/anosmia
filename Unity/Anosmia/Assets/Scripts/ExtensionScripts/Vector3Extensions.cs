﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Vector3Extensions
{
    public static bool IsPointInCameraBounds2D(this Vector3 point, Camera camera)
    {
        Vector3 tCameraPoint = camera.WorldToScreenPoint(point);
        return tCameraPoint.x > 0 &&
                tCameraPoint.y > 0 &&
                tCameraPoint.z > camera.nearClipPlane &&
                tCameraPoint.x < camera.pixelWidth &&
                tCameraPoint.y < camera.pixelHeight &&
                tCameraPoint.z < camera.farClipPlane;
    }
}
