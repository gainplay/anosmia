﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class GameInitialization : MonoBehaviour
{
    int autenticateTries = 0;
#if UNITY_IOS
    public static bool mergedOldANdNewID;
#endif
    void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        SocialController.initializeGoogle();
        autenticateTries++;
#endif

#if UNITY_ANDROID || UNITY_IOS && !UNITY_EDITOR
        SocialController.Autenticate(b => { if (b == true) { StartCoroutine(StartContinued()); } else { RetryAutenticate(); } });


#elif UNITY_EDITOR
        StartCoroutine(StartContinued());
        mergedOldANdNewID = true;
#endif


    }

    void RetryAutenticate()
    {
        autenticateTries++;
        Debug.Log($"autenticate tries = {autenticateTries}");
        SocialController.Autenticate(b => { if (b == true) { StartCoroutine(StartContinued()); } else { RetryAutenticate(); } });

    }

    IEnumerator StartContinued()
    {
        Debug.Log("start while loop");
        while (!SocialController.TryGetUniqueID(out string uniqueId))
        {
            Debug.Log("stuck?");
            yield return new WaitForEndOfFrame();
        }

#if UNITY_IOS && !UNITY_EDITOR
        Debug.Log($"old id = {((UnityEngine.SocialPlatforms.Impl.UserProfile)Social.localUser).legacyId}, new id = {Social.localUser.id}");
        yield return MergeOldAndNewIDIOS();
            Debug.Log("actual out of coroutine");

#endif

        Debug.Log("not stuck");
        yield return GetInitialData.GetInitialDataFromDB(SocialController.GetUniqueID, HasInitialDataCallback);
    }

    IEnumerator MergeOldAndNewIDIOS() 
    {
#if UNITY_IOS
        WWWForm form = new WWWForm();
        form.AddField("oldId", ((UnityEngine.SocialPlatforms.Impl.UserProfile)Social.localUser).legacyId);
        form.AddField("newId", Social.localUser.id);
        UnityWebRequest req = UnityWebRequest.Post(DatabaseHelper.baseUrl + "mergeOldAndNewIDIOS.php", form);
        yield return req.SendWebRequest();
        if (string.IsNullOrEmpty(req.error) && !req.downloadHandler.text.Contains("error"))
        {
            Debug.Log("no error");
            if (bool.TryParse(req.downloadHandler.text, out bool merged))
            {
                Debug.Log($"merged = {merged}");
                mergedOldANdNewID = merged;
            }
            else
            {
                Debug.Log($"didn get bool got: {req.downloadHandler.text}");

            }
        }
        else
        {
            Debug.Log($"error, www error = {req.error}, text body = {req.downloadHandler.text}");
        }
        Debug.Log("end of coroutine");

#endif
        yield return 0;
    }

    void HasInitialDataCallback(bool hasData)
    {
        if (hasData)
        {
            Debug.Log("load all data");
            StartCoroutine(LoadRemainingData());
        }
        else
        {
            Debug.Log("load game scene data");

            LoadGameScene();
        }
    }


    IEnumerator LoadRemainingData()
    {
        yield return LoadMinigameResults.LoadMinigameResultsFromDatabase(SocialController.GetUniqueID,l => MinigameResultController.minigameResults = l);
        yield return LoadDiaryItems.LoadDiaryItemsFromDatabase(SocialController.GetUniqueID, l=> DiaryController.diaryItems = l);

        LoadGameScene();
    }

    void LoadGameScene()
    {
        SceneManager.LoadSceneAsync(1, LoadSceneMode.Single);
    }

}
