﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuInitialization : MonoBehaviour
{
    [SerializeField]
    GameObject _questionaireStart;
    [SerializeField]
    GameObject[] _GameobjectsToDisableIfThereIsNoQuestionaire;
    [SerializeField]
    AudioListener audioListener;
    [SerializeField]
    ToggleGroup _menuToggleGroup;
 

    void Start()
    {
        EnableQuestionaireOrNot(SettingsHolder.birthYear == 0);
        audioListener.enabled = PlayerPrefs.HasKey("playSound") ?  PlayerPrefs.GetInt("playSound") != 0 : true;
    }

    /// <summary>
    /// if the questionaire is shown some menu items get turned off and the is turned on
    /// inverted if questionaure doesnt have to be shown.
    /// </summary>
    /// <param name="enableQuestionaire">the bool the enable the questionaure or not</param>
    public void EnableQuestionaireOrNot(bool enableQuestionaire)
    {
        _menuToggleGroup.allowSwitchOff = enableQuestionaire;
        _questionaireStart.SetActive(enableQuestionaire);

        foreach (var item in _GameobjectsToDisableIfThereIsNoQuestionaire)
        {
            item.SetActive(!enableQuestionaire);
        }
    }
}
