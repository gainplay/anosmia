﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class DropDownHelper
{

    public static string ValueText(this Dropdown dropdown)
    {
        return dropdown.options[dropdown.value].text;
    }
}
