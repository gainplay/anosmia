﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class JSonHelper
{
    /// <summary>
    /// converts the list to JSON by putting it in a wrapper class so the JsonUtility can peroperly create a json string out of it
    /// </summary>
    /// <typeparam name="T"> type T in listT</typeparam>
    /// <param name="list">the list to create a json from</param>
    /// <returns></returns>
    public static string ToJson<T>(this List<T> list)
    {
        return JsonUtility.ToJson(new JsonListWrapper<T>() { wrapper = list });
    }

    /// <summary>
    /// clears the list and then fills it with the elements provided by the json
    /// can only be used if the list is already initialized
    /// </summary>
    /// <typeparam name="T">the type T in the list</typeparam>
    /// <param name="list">the list that is getting filled with elements</param>
    /// <param name="json">the json to create the elementsw from</param>
    public static void FromJson<T>(this List<T> list, string json)
    {
        list.Clear();
        Debug.Log($"json = {json}");
        list.AddRange(JsonUtility.FromJson<JsonListWrapper<T>>(json).wrapper);
    }

    public static List<T> NewListFromJson<T>(string json)
    {
        return JsonUtility.FromJson<JsonListWrapper<T>>(json).wrapper;
    }

    /// <summary>
    /// the wrapper class to handle the conversion to and from json
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonListWrapper<T>
    {
        public List<T> wrapper = new List<T>();
    }
}
