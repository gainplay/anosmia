﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SmellSetHelper
{
    public static string[] GetSMellSet(int smellSetIndex)
    {
        switch (smellSetIndex)
        {
            default:
            case 0:
                return new string[] { "roos", "eucalyptus", "kruidnagel", "citroen" };
            case 1:
                return new string[] { "mentol", "tijm", "mandarijn", "jasmijn" };
            case 2:
                return new string[] { "groene thee", "bergamot", "rozemarijn", "gardenia" };
        }
    }


    public static Smells GetSmellEnum(int smellIndex, int smellSetIndex)
    {
        if (smellSetIndex == 0)
        {
            switch (smellIndex)
            {
                default:
                case 1: return Smells.roos;
                case 2: return Smells.eucalyptus;
                case 3: return Smells.kruidnagel;
                case 4: return Smells.citroen;
            }
        }
        else if (smellSetIndex == 1)
        {
            switch (smellIndex)
            {
                default:
                case 1: return Smells.mentol;
                case 2: return Smells.tijm;
                case 3: return Smells.mandarijn;
                case 4: return Smells.jasmijn;
            }
        }
        else
        {
            switch (smellIndex)
            {
                default:
                case 1: return Smells.groeneThee;
                case 2: return Smells.bergamot;
                case 3: return Smells.rozemarijn;
                case 4: return Smells.gardenia;
            }
        }
    }
}



public enum Smells
{
    roos, eucalyptus, kruidnagel, citroen,
    mentol, tijm, mandarijn, jasmijn,
    groeneThee, bergamot, rozemarijn, gardenia
}

//set 1: roos, eucalyptus, kruidnagel & citroen
//set 2: mentol, tijm, mandarijn & jasmijn
//set 3: groene thee, bergamot, rozemarijn & gardenia