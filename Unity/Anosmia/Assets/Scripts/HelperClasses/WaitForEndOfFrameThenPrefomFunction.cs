﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class WaitForEndOfFrameThenPrefomFunction
{
    public static IEnumerator WaitForEndOfFrameThenAction(Action action)
    {
        yield return new WaitForEndOfFrame();
        action?.Invoke();
    }
}
