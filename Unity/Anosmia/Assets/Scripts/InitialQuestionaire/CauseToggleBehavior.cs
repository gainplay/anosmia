﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CauseToggleBehavior : MonoBehaviour
{
    [SerializeField]
    int index;
    [SerializeField]
    QuestionaireController controller;

    public void OnValueChanged(bool targetState)
    {
        if (targetState)
        {
            controller.selectedAnosmiaCauses.Add(index);
        }
        else
        {
            controller.selectedAnosmiaCauses.Remove(index);

        }
    }
}
