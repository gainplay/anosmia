﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentSmellAbilityToggleBehavior : MonoBehaviour
{
    [SerializeField]
    int index;
    [SerializeField]
    QuestionaireController controller;

    public void OnValueChanged(bool targetState)
    {
        if (targetState)
        {
            controller.currentSmellAbility = index;
        }
    }
}
