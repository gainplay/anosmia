﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperienceSmellTrainingToggleBehavior : MonoBehaviour
{
    [SerializeField]
    int index;
    [SerializeField]
    QuestionaireController controller;

    public void OnValueChanged(bool targetState)
    {
        if (targetState)
        {
            controller.trainingExperience = index;
        }
    }
}
