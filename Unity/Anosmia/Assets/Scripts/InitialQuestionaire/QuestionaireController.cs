﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class QuestionaireController : MonoBehaviour
{
    [SerializeField]
    private Toggle _permissionToggle;
    [SerializeField]
    private Toggle _birthYearEnabledToggle;
    [SerializeField]
    private Dropdown _birthYearDropDown;
    /// <summary>
    /// 1male;2female;3other;0 vertel ik liever niet
    /// </summary>
    public int selectedGender;
    public List<int> selectedAnosmiaCauses;
    [SerializeField]
    private Toggle _diagnosedByDoctorToggle;
    [SerializeField]
    private Dropdown _sinceWhenMonthDropdown, _sinceWhenYearDropDown;
    [SerializeField]
    private Toggle dontKnowFromWhenIHaveAnosmia;
    /// <summary>
    /// 0 no, 1 yes_fewdays, 2 yes_3_months, 3 yes_6_months, 4 yes_9_months
    /// </summary>
    public int trainingExperience;
    /// <summary>
    ///0 complete, 1 hyposmia serious, 2 hyposmia average, 3 hyposmia light, 4 parosmia, 5 fantosmia, 6 no anosmia
    /// </summary>
    public int currentSmellAbility;
    [SerializeField]
    GameObject[] _gameObjectsToTurnOnAfterQuestionaire;
    [SerializeField]
    ToggleGroup _menuToggleGroup;


    public void StartSendResultToDB()
    {
        StartCoroutine(SendResultToDB());
    }

    public IEnumerator SendResultToDB()
    {
        yield return SendInitialData.SendInitialDataToDatabase
            (
                new SendInitialDataStruct
                    (
                        SocialController.GetUniqueID,
                        _permissionToggle.isOn,
                        _birthYearEnabledToggle.isOn && _birthYearDropDown.value != 0 ? int.Parse(_birthYearDropDown.options[_birthYearDropDown.value].text) : -2,
                        selectedGender,
                        CausesToDBString(),
                        _diagnosedByDoctorToggle.isOn,
                        dontKnowFromWhenIHaveAnosmia.isOn ? null : AnosmiaFromDateDropdownsToDBString(),
                        trainingExperience,
                        currentSmellAbility

                    ),
                SendDataCallback
            );
    }

    public void SetGender(int genderIndex)
    {
        selectedGender = genderIndex;
    }
    private void SendDataCallback(bool success)
    {
        if (success)
        {
            StartCoroutine(GetInitialDataCoroutine());
        }
        else
        {
            //retry
            StartSendResultToDB();
        }
    }

    IEnumerator GetInitialDataCoroutine()
    {
        yield return GetInitialData.GetInitialDataFromDB(SocialController.GetUniqueID,OpenMainMenuOnSuccess);
    }

    private void OpenMainMenuOnSuccess(bool success)
    {
        if (success)
        {
            //close questionaire
            gameObject.SetActive(false);
            foreach (var item in _gameObjectsToTurnOnAfterQuestionaire)
            {
                item.SetActive(true);
            }
            _menuToggleGroup.allowSwitchOff = false;

        }
        else
        {
            //retry
            StartCoroutine(GetInitialDataCoroutine());
        }
    }

    private string CausesToDBString()
    {
        string tResult = "";
        foreach (var item in selectedAnosmiaCauses)
        {
            tResult += $"{item},";
        }
        tResult = tResult.TrimEnd(',');
        return tResult;
    }

    private string AnosmiaFromDateDropdownsToDBString()
    {
        if (_sinceWhenYearDropDown.value == 0 || _sinceWhenMonthDropdown.value == 0)
        {
            return null;
        }
        Debug.Log($"input = {$"{_sinceWhenYearDropDown.ValueText()}-{_sinceWhenMonthDropdown.ValueText()}"}, output = {DateTime.ParseExact($"{_sinceWhenYearDropDown.ValueText()}-{_sinceWhenMonthDropdown.ValueText()}","yyyy-MMMM",new CultureInfo("nl-NL")).ToString("yyyy-MM")}");
        return DateTime.ParseExact($"{_sinceWhenYearDropDown.ValueText()}-{_sinceWhenMonthDropdown.ValueText()}","yyyy-MMMM",new CultureInfo("nl-NL")).ToString("yyyy-MM-dd");
    }
}
