﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleButtonActivityBasedOnTogglegroups : MonoBehaviour
{
    [SerializeField]
    ToggleGroup[] toggleGroups;
    [SerializeField]
    Toggle[] togglesWithoutToggleGroup;
    [SerializeField]
    Button buttonToSetActivityFrom;


    private void Update()
    {
        for (int i = 0; i < toggleGroups.Length; i++)
        {
            if (!toggleGroups[i].AnyTogglesOn())
            {
                buttonToSetActivityFrom.interactable = false;
                return;
            }
        }

        if (togglesWithoutToggleGroup.Length > 0)
        {
            if (!AnyToggleOfArrayOn(togglesWithoutToggleGroup))
            {
                buttonToSetActivityFrom.interactable = false;
                return;
            }
        }
        buttonToSetActivityFrom.interactable = true;
        
    }

    bool AnyToggleOfArrayOn(Toggle[] toggles)
    {
        for (int i = 0; i < toggles.Length; i++)
        {
            if (toggles[i].isOn)
            {
                return true;
            }
        }

        return false;
    }
}
