﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class ImageController : MonoBehaviour
{
    [SerializeField]
    Image animatedImage;
    [SerializeField]
    List<Images> images;
    private void OnEnable()
    {
        animatedImage.sprite = (from item in images where item.smell == SmellSetHelper.GetSmellEnum(SelectMinigame.selectedSmell, SettingsHolder.smellSetSelected) select item.sprites).First().ToArray().Random();
    }


    [System.Serializable]
    struct Images {public  List<Sprite> sprites; public Smells smell; }
}
