﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatedImageController : SimpleTimerController
{
    [SerializeField]
    GameObject postGameScreenButton;

    protected override void OnEnable()
    {
        base.OnEnable();
        postGameScreenButton.SetActive(false);
    }

    /// <summary>
    /// overridden to show the show postgame screen button instead
    /// </summary>
    protected override void ShowPostGameScreen()
    {
        postGameScreenButton.SetActive(true);
    }

    /// <summary>
    /// shows the postgame screen from a button
    /// </summary>
    /// <param name="minigameIndex"></param>
    public void ShowPostGameScreen(int minigameIndex)
    {
        postGameScreen.Init(minigameIndex);
    }
}
