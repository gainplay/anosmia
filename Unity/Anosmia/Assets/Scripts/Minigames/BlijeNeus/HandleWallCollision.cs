﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// hold a variable for when the happy nose hits one of the walls
/// </summary>
public class HandleWallCollision : MonoBehaviour
{
    public Vector2 moveventMultiplierChange= new Vector2(-1,1);
}
