﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HappyNoseBGItem : MonoBehaviour, iMoveMe
{

    bool _firstDeltaPosSet;
    Vector3 _deltaPosition;
    Vector3 _currPosition => transform.position;
    [SerializeField]
    Camera _mainCamera;
    HappyNoseController _blijeNeusController;
    [SerializeField]
    SpriteRenderer[] _renderers;

    public void Init(Camera mainCamera, HappyNoseController blijeNeusController)
    {
       
        _deltaPosition = _currPosition;
        _mainCamera = mainCamera;
        _blijeNeusController = blijeNeusController;
        enabled = true;
    }

    public void Init(Camera mainCamera, HappyNoseController blijeNeusController, int sortingLayer)
    {
        if (_renderers.Length == 0)
        {
            _renderers = GetComponentsInChildren<SpriteRenderer>();
        }
        _deltaPosition = _currPosition;
        _mainCamera = mainCamera;
        _blijeNeusController = blijeNeusController;
        foreach (SpriteRenderer item in _renderers)
        {
            item.sortingOrder = sortingLayer;
        }
        enabled = true;
    }

    public void MoveMe(Vector3 velocity)
    {
        transform.Translate(velocity);
    }

    private void Update()
    {
        if (_deltaPosition != _currPosition && !IsPointInCameraBounds2D(_deltaPosition) && IsPointInCameraBounds2D(_currPosition))
        {
            enabled = false;
            _blijeNeusController.SpawnBackgroundItem();
        }
        _deltaPosition = _currPosition;
    }

    private bool IsPointInCameraBounds2D(Vector3 point)
    {
        Vector3 tCameraPoint = _mainCamera.WorldToScreenPoint(point);
        return tCameraPoint.x > 0 &&
                tCameraPoint.y > 0 &&
                tCameraPoint.z > _mainCamera.nearClipPlane &&
                tCameraPoint.x < _mainCamera.pixelWidth &&
                tCameraPoint.y < _mainCamera.pixelHeight &&
                tCameraPoint.z < _mainCamera.farClipPlane;
    }
}
