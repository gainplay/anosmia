﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HappyNoseController : SimpleTimerController
{
    [SerializeField]
    GameObject[] tierOne;
    [SerializeField]
    GameObject[] tierTwo;
    [SerializeField]
    GameObject[] tierThree;
    [SerializeField]
    GameObject[] garden;
    GameObject _spawnPosition;
    bool _spawnCurveLeftHigh = true;
    [SerializeField]
    Camera _mainCamera;
    [SerializeField]
    List<iMoveMe> _bgItems = new List<iMoveMe>();
    Transform _bgItemsParent;
    [SerializeField]
    HappyNoseObjectHolder _bgItemParentPRefab;
    int _currSortingLayerLastBGItem;
    HappyNoseMovement _noseToShoot;
    ParticleSystem _noseParicles;
    Vector2 _velocity, _baseVelocity;
    GameObject _catapultCenter;
    LineRenderer[] _cataPultLines;
    [SerializeField]
    HappyNoseRock _rockPrefab;
    [SerializeField]
    float _rockSpawnBaseCooldown;
    float _currentRockSpawnCooldown;
    private Transform _spawnRockLeft, _spawnRockRight;
    GameObject _target;
    [SerializeField]
    ParticleSpritesController _particleSprites;
    bool _primed;
    GameObject cataPultTarget;

    enum CurrentTier
    {
        One,
        Two,
        Three,
        Garden
    }

    CurrentTier _currentTier = CurrentTier.One;
    private bool _notFired = true;
    [SerializeField]
    private float moveSpeedMultilier = 1;

    /*
    private void Awake()
    {
        HappyNoseBGItem[] happyNoseBGItems = _bgItemsParent.GetComponentsInChildren<HappyNoseBGItem>();
        foreach (Transform item in _bgItemsParent)
        {
            iMoveMe titem = item.GetComponent<iMoveMe>();
            if (titem != null && item.GetComponent<HappyNoseMovement>() == null)
            {
                _bgItems.Add(titem);
            }
        }
        for (int i = 0; i < happyNoseBGItems.Length; i++)
        {
            happyNoseBGItems[i].Init(_mainCamera, this);
        }
        _currSortingLayerLastBGItem = -_bgItems.Count;
        noseToShoot.YPosRemnant += MoveBackgroundItems;
    }
    */
    
   
    public void Init()
    {
        DestroyBGParent();
        HappyNoseObjectHolder tObjects = Instantiate(_bgItemParentPRefab);
        _cataPultLines = tObjects.cataPultLines;
        _catapultCenter = tObjects.catapultCenter;
        _noseToShoot = tObjects.noseToMove;
        cataPultTarget = tObjects.cataPultTarget;
        _noseParicles = _noseToShoot.GetComponent<ParticleSystem>();
        Sprite[] particles = _particleSprites.GetParticleSprites(SettingsHolder.smellSetSelected, SelectMinigame.selectedSmell);
        _noseParicles.textureSheetAnimation.SetSprite(0,particles[0]);
        _noseParicles.textureSheetAnimation.SetSprite(1,particles[1]);
        _spawnPosition = tObjects.bgItemSpawnPos;
        _bgItemsParent = tObjects.transform;
        _spawnRockLeft = tObjects.rockSpawnLeft.transform;
        _spawnRockRight = tObjects.rockSpawnRight.transform;
        _target = tObjects.target;
        _bgItems.Clear();
        HappyNoseBGItem[] happyNoseBGItems = _bgItemsParent.GetComponentsInChildren<HappyNoseBGItem>();
        foreach (Transform item in _bgItemsParent)
        {
            iMoveMe titem = item.GetComponent<iMoveMe>();
            if (titem != null && item.GetComponent<HappyNoseMovement>() == null)
            {
                _bgItems.Add(titem);
            }
        }
        for (int i = 0; i < happyNoseBGItems.Length; i++)
        {
            happyNoseBGItems[i].Init(_mainCamera, this);
        }
        _currSortingLayerLastBGItem = -_bgItems.Count;
        _noseToShoot.YPosRemnant += MoveBackgroundItems;
        _notFired = true;
        _currentRockSpawnCooldown = _rockSpawnBaseCooldown;
        _target.SetActive(true);
        _target.SetPosition(new Vector3(Mathf.Lerp(_spawnRockLeft.position.x, _spawnRockRight.position.x, Random.value), _target.GetPosition().y, _target.GetPosition().z));
        TogglePregameScreen(false);
    }

    public void DestroyBGParent()
    {
        if (_bgItemsParent != null)
        {
            Destroy(_bgItemsParent.gameObject);
        }
    }

    public override void StartMiniGame()
    {
        _primed = false;
        base.StartMiniGame();
        _currentTier = CurrentTier.One;
        _target.SetActive(false);
    }

    protected override void Update()
    {
        base.Update();
        if (_timeLeft.Day > 1 && _timeLeft.Second < 2)
        {
            _currentTier = CurrentTier.Garden;
        }
        else if (_timeLeft.Day > 1 && _timeLeft.Second < 8)
        {
            _currentTier = CurrentTier.Three;
        }
        else if (_timeLeft.Day > 1 && _timeLeft.Second < 14)
        {
            _currentTier = CurrentTier.Two;
        }

        if (_notFired && preGameAssets[0].activeSelf == false)
        {
            if (Input.GetMouseButtonDown(0))
            {
                _primed = true;
            }
            if (Input.GetMouseButton(0) && _primed)
            {
                Vector3 tNosePos = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
                tNosePos.z = 0;
                _noseToShoot.Position = tNosePos;
                Vector3 tPosition = cataPultTarget.transform.position;
                tPosition.z = 0;
                if (tPosition.y > _cataPultLines[0].transform.position.y)
                {
                    tPosition.y = _cataPultLines[0].transform.position.y;
                }
                //_noseToShoot.Position = tPosition;
                for (int i = 0; i < _cataPultLines.Length; i++)
                {
                    _cataPultLines[i].SetPosition(1, tPosition);
                }
            }
            if (Input.GetMouseButtonUp(0) && _primed)
            {
                _notFired = false;
                CalculateVelocity();
                _noseToShoot.ResetMoveMultiplier();
                StartMiniGame();
                _noseToShoot.animator.SetTrigger("Play");
            }
        }
        if (!_notFired && _timeLeft > _timeUp)
        {
            _noseToShoot.MoveMe(_velocity * Time.deltaTime * moveSpeedMultilier);

            //spawns a rock if needed
            _currentRockSpawnCooldown -= Time.deltaTime;
            if (_currentRockSpawnCooldown <= 0)
            {
                SpawnRock();
                _currentRockSpawnCooldown = _rockSpawnBaseCooldown;
            }

            //moves the catapult lines
            if (_noseToShoot.Position.y < _cataPultLines[0].transform.position.y)
            {
                for (int i = 0; i < _cataPultLines.Length; i++)
                {
                    _cataPultLines[i].SetPosition(1, _noseToShoot.Position);
                }
            }

            happyNoseFog.SetTargetAlpha(Mathf.InverseLerp(0,20, _timeLeft.Second));
        }


    }

    void MoveBackgroundItems(float velocity)
    {
        for (int i = _bgItems.Count - 1; i >= 0; i--)
        {
            if (_bgItems[i] == null)
            {
                _bgItems.RemoveAt(i);
                continue;
            }
            _bgItems[i].MoveMe(Vector3.down * velocity);
        }
    }

    private void CalculateVelocity()
    {
        _velocity = _catapultCenter.transform.position - _noseToShoot.transform.position;
        _baseVelocity = _velocity;
    }

    public void SpawnBackgroundItem()
    {
        GameObject objectToSpawn;
        switch (_currentTier)
        {
            case CurrentTier.One:
                objectToSpawn = tierOne.Random();
                break;
            case CurrentTier.Two:
                objectToSpawn = tierTwo.Random();
                break;
            case CurrentTier.Three:
                objectToSpawn = tierThree.Random();
                break;
            case CurrentTier.Garden:
                objectToSpawn = garden.Random();
                break;
            default:
                objectToSpawn = tierThree.Random();
                break;
        }
        Vector3 tSpawnPos = _spawnPosition.transform.position + Vector3.right * (_spawnCurveLeftHigh ? -1 : 1);
        HappyNoseBGItem tBgItem = Instantiate(objectToSpawn, tSpawnPos, objectToSpawn.transform.rotation, _bgItemsParent).GetComponent<HappyNoseBGItem>();
        tBgItem.Init(_mainCamera, this, --_currSortingLayerLastBGItem);
        _bgItems.Add(tBgItem);
        _spawnCurveLeftHigh = !_spawnCurveLeftHigh;
    }


    public void SpawnRock()
    {
        Vector3 tSpawnPos = new Vector3(Vector3.Lerp(_spawnRockLeft.position, _spawnRockRight.position, Random.value).x, _spawnPosition.GetPosition().y, 0);
        HappyNoseRock tRock = Instantiate(_rockPrefab, tSpawnPos, Quaternion.identity, _bgItemsParent);
        _bgItems.Add(tRock);
    }

    protected override void ShowPostGameScreen()
    {
        postGameScreen.Init(0,_noseToShoot.bounceCount);
        DestroyBGParent();
    }
}
