﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this script takes over the movement as soon as the nose is released from the catapult
/// </summary>
public class HappyNoseMovement : MonoBehaviour, iMoveMe
{
    public int bounceCount;
    public float yMovement;
    Vector2 _movementpultiplier = new Vector2(1,1);

    float _maxYPos;
    Vector3 _myPos => transform.position; 
    public Vector3 Position { get { return _myPos; } set { transform.position = value; } }
    Vector3 _deltaPosition;
    public Animator animator;
    [SerializeField]
    ParticleSystem _paricleSystem;

    /// <summary>
    /// returns the remant Y movement Once the nose reaches it's max height
    /// </summary>
    public Action<float> YPosRemnant;

    private void Awake()
    {
        _deltaPosition = _myPos;
    }

    public void ResetMoveMultiplier()
    {
        _movementpultiplier = new Vector2(1,1);
    }

    public void MoveMe(Vector3 velocity)
    {
        //Debug.Log(velocity);
        velocity.z = 0;
        yMovement += velocity.y;
        if (_myPos.y >= _maxYPos)
        {
            YPosRemnant(velocity.y);
            velocity.y = 0;
            transform.Translate(velocity * _movementpultiplier,Space.World);
        }
        else
        {
            transform.Translate(velocity * _movementpultiplier, Space.World);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log($"collide other .name = {collision.collider.name}");
        HandleWallCollision tCollisionHandler = collision.gameObject.GetComponent<HandleWallCollision>();
        if (tCollisionHandler != null)
        {
            _paricleSystem.Play();
            bounceCount++;
            _movementpultiplier *= tCollisionHandler.moveventMultiplierChange;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log($"collide other .name = {collision.name}");
        HandleWallCollision tCollisionHandler = collision.gameObject.GetComponent<HandleWallCollision>();
        if (tCollisionHandler != null)
        {
            _paricleSystem.Play();
            bounceCount++;
            _movementpultiplier *= tCollisionHandler.moveventMultiplierChange;
        }
    }
}
