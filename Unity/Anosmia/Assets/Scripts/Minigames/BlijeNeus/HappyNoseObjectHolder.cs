﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HappyNoseObjectHolder : MonoBehaviour
{
    public GameObject catapultCenter;
    public HappyNoseMovement noseToMove;
    public LineRenderer[] cataPultLines;
    public GameObject bgItemSpawnPos;
    public GameObject rockSpawnLeft, rockSpawnRight;
    public GameObject target;
    public GameObject cataPultTarget;
}
