﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HappyNoseRock : MonoBehaviour, iMoveMe
{
    public void MoveMe(Vector3 velocity)
    {
        transform.Translate(velocity);
    }
}
