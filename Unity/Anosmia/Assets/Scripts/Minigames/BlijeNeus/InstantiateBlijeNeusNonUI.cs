﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateBlijeNeusNonUI : MonoBehaviour
{
    GameObject prefab;
    HappyNoseController _happyNoseController;

    public void InstantiateAndLinkPrefab()
    {
        Instantiate(prefab);
    }
}
