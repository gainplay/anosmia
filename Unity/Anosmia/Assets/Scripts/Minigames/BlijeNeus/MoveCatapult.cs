﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCatapult : MonoBehaviour, iMoveMe
{
    [SerializeField]
    LineRenderer _lineRenderer;

    public void MoveMe(Vector3 velocity)
    {
        transform.Translate(velocity);
        _lineRenderer.SetPosition(0,_lineRenderer.GetPosition(0) + velocity);
        _lineRenderer.SetPosition(1, _lineRenderer.GetPosition(1) + velocity);
    }
}
