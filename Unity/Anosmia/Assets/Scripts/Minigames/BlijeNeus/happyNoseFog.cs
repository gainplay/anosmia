﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class happyNoseFog : MonoBehaviour , iMoveMe
{
    [SerializeField]
    Transform fogSpawn;
    [SerializeField]
    Transform hillSpawn;
    [SerializeField]
    bool hasSpawned;
    [SerializeField]
    happyNoseFog prefab;
    happyNoseFog child;
    [SerializeField]
    SpriteRenderer sprite;
    static float _targetAlpha = 1;


    IEnumerator Start()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();

            if (sprite.color.a < _targetAlpha)
            {
                float tAlpha = sprite.color.a;
                tAlpha += Time.deltaTime;
                tAlpha = tAlpha > _targetAlpha ? _targetAlpha : tAlpha;
                child?.SetAlpha(tAlpha);
            }
            else if (sprite.color.a > _targetAlpha)
            {
                float tAlpha = sprite.color.a;
                tAlpha += Time.deltaTime;
                tAlpha = tAlpha < _targetAlpha ? _targetAlpha : tAlpha;
                child?.SetAlpha(tAlpha);
            } 
        }
    }

    private void Update()
    {
        SetAlpha(_targetAlpha);
    }

    public void MoveMe(Vector3 velocity)
    {
        transform.Translate(velocity);
        child?.MoveMe(velocity);
        if (transform.position.y < hillSpawn.position.y && !hasSpawned)
        {
            SpawnFog();
        }
    }

    public static void SetTargetAlpha(float newAlpha)
    {
        _targetAlpha = newAlpha;
    }

    public void SetAlpha(float newAlpha)
    {
        Color t = sprite.color;
        t.a = newAlpha;
        sprite.color = t;
    }




    private void SpawnFog()
    {
        happyNoseFog t = Instantiate(prefab,fogSpawn.position,Quaternion.identity,transform.parent);
        t.hillSpawn = hillSpawn;
        child = t;
        hasSpawned = true;
    }
}
