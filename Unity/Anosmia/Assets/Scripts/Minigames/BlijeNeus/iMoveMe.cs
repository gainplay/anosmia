﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iMoveMe
{
    void MoveMe(Vector3 velocity);
}