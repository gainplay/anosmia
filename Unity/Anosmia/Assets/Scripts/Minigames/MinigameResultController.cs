﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameResultController : MonoBehaviour
{
    public static List<MinigameResultItem> minigameResults = new List<MinigameResultItem>();

    DiaryItem currentItem;

    public static IEnumerator LoadMinigameResults()
    {
        yield return global::LoadMinigameResults.LoadMinigameResultsFromDatabase(SocialController.GetUniqueID, (List<MinigameResultItem> l) => minigameResults = l);
        minigameResults.Sort((x, y) => { return x.trainingTime.CompareTo(y.trainingTime); });
    }
}
