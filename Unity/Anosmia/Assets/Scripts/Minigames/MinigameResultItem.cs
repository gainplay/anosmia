﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MinigameResultItem
{
    public int smell;
    public int smellSet;
    public int minigame;
    public DateTime trainingTime;

    public MinigameResultItem()
    {
    }

    public MinigameResultItem(MinigameResultJsonBridge jsonBridge)
    {
        ///Debug.Log(jsonBridge);
        string[] smellAndSetSplit = jsonBridge.smell.Split('-');
        this.smellSet = int.Parse(smellAndSetSplit[0]);
        this.smell = int.Parse(smellAndSetSplit[1]) ;
        this.minigame = jsonBridge.minigame;
        this.trainingTime = DateTime.Parse(jsonBridge.trainingTime);
    }

    public MinigameResultJsonBridge GetJsonBridgeItem {get{ return new MinigameResultJsonBridge(this); } }
}


/// <summary>
/// a stuct meant to help convert the MinigameResultItem to json as unity can't serialize datetimes
/// </summary>
[Serializable]
public struct MinigameResultJsonBridge
{
    public string smell;
    public int minigame;
    public string trainingTime;

    public MinigameResultJsonBridge(MinigameResultItem minigameResult)
    {
        this.smell = $"{minigameResult.smellSet}-{minigameResult.smell}";
        this.minigame = minigameResult.minigame;
        this.trainingTime = minigameResult.trainingTime.ToString();
    }

    public override string ToString()
    {
        return $"smell = {smell}, minigame = {minigame}, trainingtime = {trainingTime}";
    }
}
