﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class ParticleSpritesController : ScriptableObject
{
    [SerializeField]
    List<ParticleSprites> particleSprites;

    public Sprite[] GetParticleSprites(int smellSet, int smell)
    {
        return particleSprites.Find(item => item.smell == smell && item.smellset == smellSet).sprites;
    }

    [System.Serializable]
    struct ParticleSprites
    {
        public Sprite[] sprites;
        public int smell;
        public int smellset;
    }
}
