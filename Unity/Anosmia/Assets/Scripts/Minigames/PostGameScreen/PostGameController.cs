﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Text.RegularExpressions;
using System;

public class PostGameController : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI scoreText;
    [SerializeField]
    string happyNoseText;
    [SerializeField]
    string smellCatcherText;
    [SerializeField]
    MainMenuController _mainMenuController;
    [SerializeField]
    TextMeshProUGUI nameOfGameText;


    public void Init(int minigameIndex)
    {
        nameOfGameText.text = GetMinigameNameFronIndex(minigameIndex);
        scoreText.gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public void Init(int minigameIndex, int score)
    {
        nameOfGameText.text = GetMinigameNameFronIndex(minigameIndex);

        string baseString = minigameIndex == 0 ? happyNoseText : smellCatcherText;
        scoreText.text = Regex.Replace(baseString, @"\{0\}", score.ToString());
        scoreText.gameObject.SetActive(true);
        gameObject.SetActive(true);
    }

    public void AnyButtonClicked()
    {
        CheckForWho();
    }

    private void CheckForWho()
    {
        if (SettingsHolder.upsit9Months != -1 && SettingsHolder.who5_9months == -1 && (DateTime.Now - SettingsHolder.startDate).Days > 270 && MinigameResultController.minigameResults.FindAll(item => item.trainingTime.Date == DateTime.Now.Date).Count >= 4)
        {
            _mainMenuController.WHO5Flag = true;

        }
        else if (SettingsHolder.upsit6Months != -1 && SettingsHolder.who5_6months == -1 && (DateTime.Now - SettingsHolder.startDate).Days > 180 && MinigameResultController.minigameResults.FindAll(item => item.trainingTime.Date == DateTime.Now.Date).Count >= 4)
        {
            _mainMenuController.WHO5Flag = true;


        }
        else if (SettingsHolder.upsit3Months != -1 && SettingsHolder.who5_3months == -1 && (DateTime.Now - SettingsHolder.startDate).Days > 90 && MinigameResultController.minigameResults.FindAll(item => item.trainingTime.Date == DateTime.Now.Date).Count >= 4)
        {
            _mainMenuController.WHO5Flag = true;

        }
        else if (SettingsHolder.upsit0Months != -1 && SettingsHolder.who5_0months == -1 && MinigameResultController.minigameResults.FindAll(item => item.trainingTime.Date == DateTime.Now.Date).Count >= 4)
        {
            _mainMenuController.WHO5Flag = true;
        }
        else
        {
            _mainMenuController.WHO5Flag = false;

        }
    }


    public string GetMinigameNameFronIndex(int minigameIndex)
    {
        switch (minigameIndex)
        {
            case 0: return "Blije neus";
            case 1: return "Geur vanger";
            case 2: return "Herinnering beschrijven";
            case 3: return "Foto animatie";
            case 4: return "Timer";
            default: return "Geen minigame";
        }
    }
}
