﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

public class TrainingIDidTodayController : MonoBehaviour
{
    List<TrainingIDidTodayEntry> entries = new List<TrainingIDidTodayEntry>();
    [SerializeField]
    TrainingIDidTodayEntry entryPrefab;
    [SerializeField]
    Transform entryParent;
    private void OnEnable()
    {
        List<MinigameResultItem[]> tList =
        (
            from
                result
            in
                MinigameResultController.minigameResults
            where
                result.trainingTime.Date == DateTime.Now.Date
            group
                result
            by new
            { result.smellSet, result.smell, result.minigame }
            into
                grp
            select
                grp.ToArray()

        ).ToList();

        for (int i = 0; i < tList.Count; i++)
        {
            if (i < entries.Count)
            {
                entries[i].Initialize(tList[i]);
            }
            else
            {
                AddEntry(tList[i]);
            }
        }
        StartCoroutine(Refesh());
    }

    IEnumerator Refesh()
    {
        yield return new WaitForEndOfFrame();
        entries.LastOrDefault()?.gameObject.SetActive(false);
        yield return new WaitForEndOfFrame();
        entries.LastOrDefault()?.gameObject.SetActive(true);
    }


    private void AddEntry(params MinigameResultItem[] items)
    {
        TrainingIDidTodayEntry tNewEntry = Instantiate(entryPrefab, entryParent);
        tNewEntry.Initialize(items);
        this.entries.Add(tNewEntry);
    }
}
