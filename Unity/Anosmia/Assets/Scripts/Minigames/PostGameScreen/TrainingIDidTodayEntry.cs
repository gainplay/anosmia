﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class TrainingIDidTodayEntry : MonoBehaviour
{
    [SerializeField]
    List<ImageHolder> images;
    [SerializeField]
    TextMeshProUGUI text;
    [SerializeField]
    Image image;



    public void Initialize(params MinigameResultItem[] results)
    {
        MinigameResultItem tItem = results[0];
        text.text = $"{results.Length}x {GetMinigameNameFromInt(tItem.minigame)}: {SmellSetHelper.GetSMellSet(tItem.smellSet)[tItem.smell-1]}";
        image.sprite = images.Find(image => image.set == tItem.smellSet && image.index == tItem.smell).image;
    }


    [System.Serializable]
    struct ImageHolder
    {
        public Sprite image;
        public int set;
        public int index;
    }

    string GetMinigameNameFromInt(int index)
    {
        switch (index)
        {
            case 0:
                return "Blije neus";
            case 1:
                return "Geur vanger";
            case 2:
                return "Herinnering beschrijven";
            case 3:
                return "Geanimeerde foto";
            case 4:
                return "Timer";
            default:
                return "Geen minigame";
        }
    }
}
