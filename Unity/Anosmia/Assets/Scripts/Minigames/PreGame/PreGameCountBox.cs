﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreGameCountBox : MonoBehaviour
{
    [SerializeField]
    int _miniGameIndex;
    [SerializeField]
    TMPro.TextMeshProUGUI _countText;
    private void OnEnable()
    {
        _countText.text = MinigameResultController.minigameResults.FindAll(m => m.minigame == _miniGameIndex).Count.ToString() + "x";
    }
}
