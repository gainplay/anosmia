﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PreGameSmellBox : MonoBehaviour
{
    [SerializeField]
    TrainingCounterIcons _icons;
    [SerializeField]
    Image _imageToSwap;
    [SerializeField]
    TextMeshProUGUI _imageName;
    private void OnEnable()
    {
        if (!SelectMinigame.noSmellSelected)
        {
            _imageName.text = SmellSetHelper.GetSMellSet(SettingsHolder.smellSetSelected)[SelectMinigame.selectedSmell - 1];
            _imageToSwap.sprite = _icons.GetIcon(SelectMinigame.selectedSmell, SettingsHolder.smellSetSelected);
            _imageToSwap.color = Color.white;
        }
        else
        {
            _imageName.text = "Geen geur";
            _imageToSwap.color = Color.clear;
        }
    }
}
