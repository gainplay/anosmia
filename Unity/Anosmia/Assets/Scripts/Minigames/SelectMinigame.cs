﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectMinigame : MonoBehaviour
{
    public static bool noSmellSelected;
    public static int selectedSmell;
    public SmellCatcherController smellCatcherController;
    public HappyNoseController happyNoseController;
    public TimerWithInputField memoryController;
    public AnimatedImageController timerWithAnimationController;
    public SimpleTimerController simpleTimerController;

    public void SetSelectedSmell(int selectedSmell)
    {
        if (selectedSmell > 0)
        {
            SelectMinigame.selectedSmell = selectedSmell;
            noSmellSelected = false;
        }
        else
        {
            SelectMinigame.selectedSmell = Random.Range(1,5);
            noSmellSelected = true;
        }
        gameObject.SetThisAndAllParentsActive(true);
    }

    public void StartSmellCatcher()
    {
        smellCatcherController.ShowPreGame();
        DisableMe();
    }
    public void StartHappyNose()
    {
        happyNoseController.ShowPreGame();
        DisableMe();
    }
    public void StartMemory()
    {
        memoryController.ShowPreGame();
        DisableMe();
    }
    public void StartAnimatedImage()
    {
        timerWithAnimationController.ShowPreGame();
        DisableMe();
    }
    public void StartTimer()
    {
        simpleTimerController.ShowPreGame();
        DisableMe();
    }

    private void DisableMe()
    {
        gameObject.SetActive(false);
    }
}
