﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectMinigameSelectedSmell : MonoBehaviour
{

    [SerializeField]
    SelectSmellIconHolder icons;
    [SerializeField]
    TMPro.TextMeshProUGUI smellName;
    [SerializeField]
    Image imageToSwap;

    private void OnEnable()
    {
        if (SelectMinigame.noSmellSelected)
        {
            imageToSwap.color = Color.clear;
            smellName.text = "Geen geur";
            Debug.Log("no smell");
        }
        else
        {
            imageToSwap.color = Color.white;
            imageToSwap.sprite = icons.smellSelectIcons[SettingsHolder.smellSetSelected].sprites[SelectMinigame.selectedSmell -1];
            smellName.text = SmellSetHelper.GetSMellSet(SettingsHolder.smellSetSelected)[SelectMinigame.selectedSmell -1];
        }
    }
}
