﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SimpleTimerController : MonoBehaviour
{
    protected DateTime _timeLeft = new DateTime().AddDays(1).AddSeconds(20);
    protected DateTime _timeUp = new DateTime().AddDays(1);
    [SerializeField]
    TimerVisualController timerVisual;

    [SerializeField]
    protected AudioSource _bellSound;
    protected bool _timeRunning;
    [SerializeField]
    protected int _minigameIndex;
    [SerializeField]
    protected PostGameController postGameScreen;
    [SerializeField]
    protected GameObject[] preGameAssets;


    public void ShowPreGame()
    {
        gameObject.SetThisAndAllParentsActive(true);
        TogglePregameScreen(true);
    }
    public void TogglePregameScreen(bool isOn)
    {
        foreach (var item in preGameAssets)
        {
            item.SetActive(isOn);
        }
    }

    public void SetMeAndParentsActive(bool target)
    {
        gameObject.SetThisAndAllParentsActive(target);
    }

    protected virtual void OnEnable()
    {
        timerVisual.Init(20);
    }

    public virtual void StartMiniGame()
    {
        _timeLeft = new DateTime().AddDays(1).AddSeconds(20);
        _timeRunning = true;
        timerVisual.StartCountDown();
        TogglePregameScreen(false);
    }

    protected virtual void Update()
    {
        if (_timeRunning)
        {
            _timeLeft = _timeLeft.AddSeconds(-Time.deltaTime);
            if (_timeLeft < _timeUp)
            {
                MiniGameFinished();
            }
        }
    }

    protected virtual void MiniGameFinished()
    {
        _timeRunning = false;
        _bellSound.Play();
        StartCoroutine(SendMiniGameFinishedToServer());
    }

    protected IEnumerator SendMiniGameFinishedToServer()
    {
        yield return SendMinigameResult.SendMinigameResultToDB(SelectMinigame.selectedSmell, _minigameIndex, b => Debug.Log($"send minigame debug success = {b}"));
        MinigameResultController.minigameResults.Add(new MinigameResultItem()
            {
                minigame = _minigameIndex,
                smell = SelectMinigame.selectedSmell,
                smellSet = SettingsHolder.smellSetSelected,
                trainingTime = DateTime.Now
            }
        );
        ShowPostGameScreen();
    }

    protected virtual void ShowPostGameScreen()
    {
        postGameScreen?.Init(_minigameIndex);
    }
}
