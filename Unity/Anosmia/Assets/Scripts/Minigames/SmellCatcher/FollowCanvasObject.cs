﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// works for screen space camera canvas
/// </summary>
public class FollowCanvasObject : MonoBehaviour
{
    [SerializeField]
    bool xAxis, yAxis;
    [SerializeField]
    RectTransform canvasObject;

    void Update()
    {
        transform.position = new Vector3(
                                            xAxis ? canvasObject.position.x : transform.position.x, 
                                            yAxis ? canvasObject.position.y : transform.position.y, 
                                            transform.position.z
                                        );
    }
}
