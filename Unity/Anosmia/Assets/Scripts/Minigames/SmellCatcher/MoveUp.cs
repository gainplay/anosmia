﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUp : MonoBehaviour
{
    [SerializeField]
    float _moveSpeed;
    public static float moveSpeedMultiplier = 1;

    void Update()
    {
        transform.Translate(Vector3.up * _moveSpeed * moveSpeedMultiplier * Time.deltaTime, Space.World);
    }
}
