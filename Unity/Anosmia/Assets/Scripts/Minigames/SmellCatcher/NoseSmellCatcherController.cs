﻿using UnityEngine;

public class NoseSmellCatcherController : MonoBehaviour
{
    Vector3 _position => transform.position;
    [SerializeField]
    SmellCatcherController _smellCatcherController;
    [SerializeField]
    Animator _animator;


    float _currentMultiplier
    {
        get { return _smellCatcherController.scoreMultiplier; }
        set { _smellCatcherController.scoreMultiplier = value; }
    }

    private void Update()
    {
        transform.position = new Vector3(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, _position.y, _position.z);
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        ScoreMultiplier tMultiplier = collision.GetComponent<ScoreMultiplier>();
        _currentMultiplier += tMultiplier == null ? 0 : tMultiplier.multiplier;
        _animator.SetTrigger("Play");
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        ScoreMultiplier tMultiplier = collision.GetComponent<ScoreMultiplier>();
        _currentMultiplier -= tMultiplier == null ? 0 : tMultiplier.multiplier;
    }
}