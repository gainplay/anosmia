﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetSmellImagesInBar : MonoBehaviour
{
    [SerializeField]
    Image ImageFull, ImageEmpty;
    [SerializeField]
    List<SpritesForSmellCatcherBar> images = new List<SpritesForSmellCatcherBar>();

    public void SetImages(int smell, int smellSet)
    {
        SpritesForSmellCatcherBar tSprites = images.Find((s) => { return s.smellIndex == smell && s.smellSet == smellSet; });
        ImageFull.sprite = tSprites.full;
        ImageEmpty.sprite = tSprites.empty;
    }

    public void SetFullImageFill(float value)
    {
        ImageFull.fillAmount = value;
    }
}


[System.Serializable]
public struct SpritesForSmellCatcherBar
{

    public Sprite full, empty;
    public int smellSet, smellIndex;
}
