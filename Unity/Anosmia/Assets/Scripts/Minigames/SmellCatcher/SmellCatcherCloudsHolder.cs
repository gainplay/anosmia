﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SmellCatcherCloudsHolder : ScriptableObject
{
    [SerializeField]
    private List<SmellCloudPrefabs> _clouds;

    public GameObject[] GetCloudsFromSmell(int smell, int smellSet)
    {
        return _clouds.Find(item => item.smell == SmellSetHelper.GetSmellEnum(smell,smellSet)).clouds;
    }


    [System.Serializable]
    struct SmellCloudPrefabs
    {
        public GameObject[] clouds;
        public Smells smell;
    }
}


