﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SmellCatcherController : SimpleTimerController
{

    [SerializeField]
    GameObject _handAnimation,  _worldItemsRoot;
    [SerializeField]
    FollowCanvasObject _noseFollowsHand;
    [SerializeField]
    NoseSmellCatcherController _noseController;
    [SerializeField]
    GameObject _leftSpawnBound, _rightSpawnBound;
    Vector3 _leftSpawnPosition => _leftSpawnBound.transform.position;
    Vector3 _rightSpawnPosition => _rightSpawnBound.transform.position;
    System.Random _lerpRandomizer = new System.Random();
    [SerializeField]
    float _spawnCoolDown;
    float _nextCloudSpawnTime;
    [SerializeField]
    GameObject[] _badClouds;
    GameObject[] _goodClouds;
    [SerializeField]
    SmellCatcherCloudsHolder _goodCloudsHolder;
    [SerializeField]
    Slider _scoreSlider;
    public float scoreMultiplier;
    [SerializeField]
    float _pointsPerSecond;
    [SerializeField]
    SetSmellImagesInBar barImagesController;
    [SerializeField]
    Sprite _eyeOpen, _eyeClosed;
    [SerializeField]
    SpriteRenderer _eyeLeft, _eyeRight;
    [SerializeField]
    GameObject _mouth;

    [SerializeField]
    float _startSpeedMultiplier, _maxSpeedOverTimeMultiplier, _fullBarSpeedMultiplier;


    public override void StartMiniGame()
    {
        base.StartMiniGame();
        _noseFollowsHand.enabled = false;
        _noseController.enabled = true;
        _handAnimation.SetActive(false);
        _scoreSlider.value = 50;
        barImagesController.SetFullImageFill(0.5f);
        SetEyesOpen(true);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        _noseController.enabled = false;
        _noseFollowsHand.enabled = true;
        _handAnimation.SetActive(true);
        _worldItemsRoot.SetActive(true);
        barImagesController.SetImages(SelectMinigame.selectedSmell, SettingsHolder.smellSetSelected);
        _goodClouds = _goodCloudsHolder.GetCloudsFromSmell(SelectMinigame.selectedSmell, SettingsHolder.smellSetSelected);
        SetEyesOpen(true);
        _mouth.SetActive(false);
    }

    protected virtual void OnDisable()
    {
        _worldItemsRoot.SetActive(false);
    }

    protected override void Update()
    {
        base.Update();
        if (_timeRunning)
        {
            if (Time.time > _nextCloudSpawnTime)
            {
                SpawnCloud();
                _nextCloudSpawnTime = Time.time + _spawnCoolDown;
            }
            if (scoreMultiplier != 0)
            {
                _scoreSlider.value += scoreMultiplier * Time.deltaTime * _pointsPerSecond;
                barImagesController.SetFullImageFill(_scoreSlider.value / 100);
                SetEyesOpen(_scoreSlider.value < 100);
                _mouth.SetActive(!(_scoreSlider.value < 100));
            }
            if (_timeLeft.Day >= 1)
            {
                if (_scoreSlider.value < 100)
                {
                    MoveUp.moveSpeedMultiplier = Mathf.Lerp(_startSpeedMultiplier, _maxSpeedOverTimeMultiplier, Mathf.InverseLerp(20, 0, _timeLeft.Second));
                    Debug.Log("movespeed multiplier " + MoveUp.moveSpeedMultiplier);
                }
                else
                {
                    MoveUp.moveSpeedMultiplier = _fullBarSpeedMultiplier;
                }
            }
        }
        else if(preGameAssets[0].activeSelf == false &&  Input.GetMouseButtonDown(0))
        {
            StartMiniGame();
        }
    }

    private void SpawnCloud()
    {
        GameObject tObjectToSpawn = UnityEngine.Random.value >= 0.4f ? _goodClouds.Random() : _badClouds.Random();
        Instantiate(tObjectToSpawn, Vector3.Lerp(_leftSpawnPosition, _rightSpawnPosition, (float)_lerpRandomizer.NextDouble()), tObjectToSpawn.transform.rotation);
    }

    protected override void MiniGameFinished()
    {
        base.MiniGameFinished();
    }

    void SetEyesOpen(bool open)
    {
        _eyeLeft.sprite = open ? _eyeOpen : _eyeClosed;
        _eyeRight.sprite = open ? _eyeOpen : _eyeClosed;
    }


    protected override void ShowPostGameScreen()
    {
        postGameScreen.Init(1,Mathf.RoundToInt(_scoreSlider.value));
        gameObject.SetActive(false);
        _worldItemsRoot.SetActive(false);
    }
}
