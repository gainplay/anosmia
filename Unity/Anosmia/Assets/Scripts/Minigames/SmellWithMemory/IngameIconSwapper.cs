﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IngameIconSwapper : MonoBehaviour
{
    [SerializeField]
    SelectSmellIconHolder icons;
    [SerializeField]
    Image imageToSwap;

    private void OnEnable()
    {
        Debug.Log($"sprite to swap to = {icons.smellSelectIcons[SettingsHolder.smellSetSelected].sprites[SelectMinigame.selectedSmell-1].name}");
        Debug.Log($"image to swap nullcheck(== null) = {imageToSwap == null}");

        imageToSwap.sprite = icons.smellSelectIcons[SettingsHolder.smellSetSelected].sprites[SelectMinigame.selectedSmell-1];
        Debug.Log("made it to after image swap");
    }
}
