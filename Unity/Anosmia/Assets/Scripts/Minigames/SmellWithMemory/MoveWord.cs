﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveWord : MonoBehaviour
{
    [SerializeField]
    private float _moveSpeed;

    void Update()
    {
        transform.Translate(Quaternion.Euler(0, 0, transform.rotation.z) * Vector3.right * _moveSpeed * Time.deltaTime);
    }
}
