﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.Serialization;
using UnityEngine.Networking;
using UnityEngine.UI;

public class TimerWithInputField : SimpleTimerController
{
    [SerializeField]
    TMPro.TMP_InputField inputField;
    [SerializeField]
    float _wordSpawnDistance;
    [SerializeField]
    TextMeshProUGUI _leftTextPrefab, _rightTextPrefab;
    [SerializeField]
    float _wordCoolDown;
    float _nextWordTime;
    [SerializeField]
    string[] _possibleWords;
    List<TextMeshProUGUI> _currentWords = new List<TextMeshProUGUI>();
    [SerializeField]
    Button saveButton;

    protected override void OnEnable()
    {
        saveButton.interactable = false;
        inputField.text = "";
        base.OnEnable();
    }

    protected override void Update()
    {
        base.Update();
        if (_timeRunning && Time.time > _nextWordTime)
        {
            //SpawnWord();
            _nextWordTime = Time.time + _wordCoolDown;
        }
    }

    private void SpawnWord()
    {
        bool tSpawnWordOnTheLeft = UnityEngine.Random.Range(0,2) == 1;
        if (tSpawnWordOnTheLeft)
        {
            TextMeshProUGUI tLeftWOrdObject = Instantiate(_leftTextPrefab,transform);
            tLeftWOrdObject.text = _possibleWords.Random();
            tLeftWOrdObject.rectTransform.anchoredPosition = new Vector2(-_wordSpawnDistance, UnityEngine.Random.Range(0, -Screen.height));
            tLeftWOrdObject.rectTransform.rotation = Quaternion.Euler(0, 0, UnityEngine.Random.Range(-45, 46));
            _currentWords.Add(tLeftWOrdObject);
        }
        else
        {
            TextMeshProUGUI tRightWOrdObject = Instantiate(_rightTextPrefab, transform);
            tRightWOrdObject.text = _possibleWords.Random();
            tRightWOrdObject.rectTransform.anchoredPosition = new Vector2(_wordSpawnDistance, UnityEngine.Random.Range(0, -Screen.height));
            tRightWOrdObject.rectTransform.rotation = Quaternion.Euler(0, 0, UnityEngine.Random.Range(-45, 46));
            _currentWords.Add(tRightWOrdObject);
        }
    }

    protected override void MiniGameFinished()
    {
        base.MiniGameFinished();

        foreach (TextMeshProUGUI item in _currentWords)
        {
            Destroy(item.gameObject);
        }
        _currentWords.Clear();
    }

    public void SaveToDiary()
    {
        if (!string.IsNullOrEmpty(inputField.text))
        {
            StartCoroutine(SaveDiaryToDatabase());
        }
        else
        {
            postGameScreen.Init(_minigameIndex);
            gameObject.SetActive(false);
        }
    }

    IEnumerator SaveDiaryToDatabase()
    {

        DiaryItem tItem = DiaryController.diaryItems.Find(d => d.date.Date == DateTime.Now.Date);
        if (tItem == null)
        {
            tItem = new DiaryItem(DateTime.Now,"",0,-1);
        }
        tItem.text += "\n\n" + inputField.text;
        WWWForm tForm = new WWWForm();
        tForm.AddField("ENTRY", tItem.text);
        UnityWebRequest tRequest = UnityWebRequest.Post(DatabaseHelper.baseUrl + "insertOrUpdateDiary.php", tForm);
        tRequest.SetRequestHeader("SMELLTODAY", tItem.smellToday.ToString());
        tRequest.SetRequestHeader("SUBPROGRESS", tItem.subProgress.ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        tRequest.SetRequestHeader("DATE", tItem.date.ToString("yyyy-MM-dd"));
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        yield return new WaitUntil(() => tRequest.isDone);

        yield return DiaryController.LoadDiaryItemsRoutine();

        postGameScreen.Init(_minigameIndex);
        gameObject.SetActive(false);
    }

    protected override void ShowPostGameScreen()
    {
        saveButton.interactable = true;
    }
}
