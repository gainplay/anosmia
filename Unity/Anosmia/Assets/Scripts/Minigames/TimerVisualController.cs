﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class TimerVisualController : MonoBehaviour
{
    [SerializeField]
    Image _fillImage;
    float _timeLeft;
    float _startTime;
    [SerializeField]
    TextMeshProUGUI _timeText;
    IEnumerator _countdownCoroutine;

    Action TimeUpEvent;

    public void Init(float startTime)
    {
        _fillImage.fillAmount = 1;
        if (startTime <= 0)
        {
            throw new System.ArgumentOutOfRangeException("The starttime has to be greater as 0");
        }
        _startTime = startTime;
        _timeLeft = startTime;
        _timeText.text = startTime.ToString();
        _fillImage.fillAmount = 0;
    }

    public void StartCountDown()
    {
        if (_countdownCoroutine != null)
        {
            StopCoroutine(_countdownCoroutine);
        }
        _countdownCoroutine = CountDownCoroutine();
        StartCoroutine(_countdownCoroutine);
    }

    IEnumerator CountDownCoroutine()
    {
        while (true)
        {
            yield return new WaitForEndOfFrame();
            _timeLeft -= Time.deltaTime;
            _fillImage.fillAmount = 1f - _timeLeft / _startTime;
            _timeText.text = Mathf.RoundToInt(_timeLeft > 0 ? _timeLeft : 0).ToString();
        }
    }
}
