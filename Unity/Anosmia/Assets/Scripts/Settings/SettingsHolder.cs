﻿using System;

public static class SettingsHolder
{
    public static bool permission;
    public static int birthYear = 0; //json parser parses NULL to 0 so we take that as default here
    public static int gender;
    public static string causeOfAnosmia;
    public static bool diagnosedByDoctor;
    public static DateTime anosmiaFrom;
    public static int anosmiaTrainingExperience = -1;
    public static int smellAbility = -1;
    public static int smellSetSelected = 0;
    public static string notificationMorningRoutine;
    public static string notificationEveningRoutine;
    public static int upsit0Months = -1;
    public static int upsit3Months = -1;
    public static int upsit6Months = -1;
    public static int upsit9Months = -1;
    public static int who5_0months = -1;
    public static int who5_3months = -1;
    public static int who5_6months = -1;
    public static int who5_9months = -1;
    public static DateTime startDate;

    public static SettingsHolderStruct dataStruct
    {
        get
        {
            return new SettingsHolderStruct(permission, birthYear, gender, causeOfAnosmia, diagnosedByDoctor, anosmiaFrom, anosmiaTrainingExperience,
                                            smellAbility, smellSetSelected,  notificationMorningRoutine, notificationEveningRoutine, upsit0Months, upsit3Months,
                                            upsit6Months, upsit9Months, who5_0months, who5_3months, who5_6months, who5_9months, startDate);
        }
        set
        {
            permission = value.permission;
            birthYear = value.birthYear;
            gender = value.gender;
            causeOfAnosmia = value.causeOfAnosmia;
            diagnosedByDoctor = value.diagnosedByDoctor;
            anosmiaFrom = !string.IsNullOrEmpty(value.anosmiaFrom) ? DateTime.ParseExact(value.anosmiaFrom, "yyyy-MM-dd", null) : default;
            anosmiaTrainingExperience = value.anosmiaTrainingExperience;
            smellAbility = value.smellAbility;
            smellSetSelected = value.smellSetSelected;
            notificationMorningRoutine = value.notificationMorningRoutine;
            notificationEveningRoutine = value.notificationEveningRoutine;
            upsit0Months = value.upsit0Months;
            upsit3Months = value.upsit3Months;
            upsit6Months = value.upsit6Months;
            upsit9Months = value.upsit9Months;
            who5_0months = value.who5_0months;
            who5_3months = value.who5_3months;
            who5_6months = value.who5_6months;
            who5_9months = value.who5_9months;
            startDate = !string.IsNullOrEmpty(value.startDate) ? DateTime.ParseExact(value.startDate, "yyyy-MM-dd", null) : new DateTime();
        }
    }
}
[Serializable]
public struct SettingsHolderStruct
{
    public bool permission;
    public int birthYear;
    public int gender;
    public string causeOfAnosmia;
    public bool diagnosedByDoctor;
    public string anosmiaFrom;
    public int anosmiaTrainingExperience;
    public int smellAbility;
    public int smellSetSelected;
    public string notificationMorningRoutine;
    public string notificationEveningRoutine;
    public int upsit0Months;
    public int upsit3Months;
    public int upsit6Months;
    public int upsit9Months;
    public int who5_0months;
    public int who5_3months;
    public int who5_6months;
    public int who5_9months;
    public string startDate;

    public SettingsHolderStruct(bool permission, int birthYear, int gender, string causeOfAnosmia, bool diagnosedByDoctor, DateTime anosmiaFrom, 
                                int anosmiaTrainingExperience, int smellAbility, int smellSetSelected, string notificationMorningRoutine, 
                                string notificationEveningRoutine, int upsit0Months, int upsit3Months, int upsit6Months, int upsit9Months, 
                                int who5_0months, int who5_3months, int who5_6months, int who5_9months, DateTime startDate)
    {
        this.permission = permission;
        this.birthYear = birthYear;
        this.gender = gender;
        this.causeOfAnosmia = causeOfAnosmia;
        this.diagnosedByDoctor = diagnosedByDoctor;
        this.anosmiaFrom = anosmiaFrom.ToString("yyyy-MM-dd");
        this.anosmiaTrainingExperience = anosmiaTrainingExperience;
        this.smellAbility = smellAbility;
        this.smellSetSelected = smellSetSelected;
        this.notificationMorningRoutine = notificationMorningRoutine;
        this.notificationEveningRoutine = notificationEveningRoutine;
        this.upsit0Months = upsit0Months;
        this.upsit3Months = upsit3Months;
        this.upsit6Months = upsit6Months;
        this.upsit9Months = upsit9Months;
        this.who5_0months = who5_0months;
        this.who5_3months = who5_3months;
        this.who5_6months = who5_6months;
        this.who5_9months = who5_9months;
        this.startDate = startDate.ToString("yyyy-MM-dd");
    }
}