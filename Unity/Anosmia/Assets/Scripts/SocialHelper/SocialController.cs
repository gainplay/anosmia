﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public static class SocialController
{

    public static bool isGoogleInitialized = false;
#if UNITY_ANDROID
    public static void initializeGoogle()
    {
        if (!isGoogleInitialized)
        {
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder().Build();
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.Activate();
            isGoogleInitialized = true;
        }
    }
#endif

    public static void Autenticate(Action<bool> callBack)
    {
#if UNITY_ANDROID || UNITY_IOS

        Social.localUser.Authenticate(callBack);
#endif
    }

    /// <summary>
    /// tries to get the unique id
    /// </summary>
    /// <param name="id">the unique id, if it fails returns null</param>
    /// <returns>true on success</returns>
    public static bool TryGetUniqueID(out string id)
    {
        if (!Application.isEditor)
        {
            Debug.Log($"autenticated = {Social.localUser.authenticated}");

            if (Social.localUser.authenticated)
            {
                id = Social.localUser.id;
                return true;
            }
            id = null;
            return false;
        }
        else
        {
            id = "Editor1337";
            return true;
        }
    }

    public static bool TryToGetUsername(out string username)
    {
        if (!Application.isEditor)
        {
            Debug.Log($"autenticated = {Social.localUser.authenticated}");
            if (Social.localUser.authenticated)
            {
                username = Social.localUser.userName;
                return true;
            }
            username = null;
            return false;
        }
        else
        {
            username = "test";
            return true;
        }
    }

    public static string GetUniqueID => !Application.isEditor ? Social.localUser.id : "test";
    public static string GetUsername => !Application.isEditor ? Social.localUser.userName : "test username";

    public static void AddScoreToLeaderBoard(string id, long score)
    {
        Social.ReportScore(score, id, success => { });
    }
    public static void ShowLeaderBoard()
    {
        Social.ShowLeaderboardUI();
    }
}
