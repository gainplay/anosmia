﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextSceneAfterDelay : MonoBehaviour
{
    [Tooltip("the delay in seconds")]
    public float delay;
    IEnumerator Start()
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(1);
    }
}
