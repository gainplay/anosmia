﻿using ChartAndGraph;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class DataVisualizationController : MonoBehaviour
{
    private GraphChartBase graphTraining;
    private GraphChartBase graphProgress;
    private TextMeshProUGUI mostPlayedMinigame;
    private TextMeshProUGUI diaryEntryCount;
    private TextMeshProUGUI maxStars;

    private void OnEnable()
    {
        graphTraining.Data.HorizontalViewOrigin = ChartDateUtility.DateToValue(DateTime.Today.AddDays(-7));
        graphTraining.DataSource.StartBatch();
        graphTraining.DataSource.ClearCategory("Player 1");
        graphTraining.DataSource.ClearCategory("Player 2");
        List<IGrouping<DateTime, MinigameResultItem>> tMinigameByDay = MinigameResultController.minigameResults.GroupBy(i => i.trainingTime.Date).ToList();
        foreach (var dayItems in tMinigameByDay)
        {
            DateTime tEveningDate = dayItems.Max(i => i.trainingTime);
            DateTime tMorningDate = dayItems.Min(i => i.trainingTime);
            graphTraining.Data.AddPointToCategory("player 1", ChartDateUtility.DateToValue(tEveningDate.Date), ChartDateUtility.TimeSpanToValue(tEveningDate.TimeOfDay));
            graphTraining.Data.AddPointToCategory("player 2", ChartDateUtility.DateToValue(tMorningDate.Date), ChartDateUtility.TimeSpanToValue(tMorningDate.TimeOfDay));
        }
        graphTraining.Data.EndBatch();

        graphProgress.DataSource.StartBatch();
        graphProgress.DataSource.ClearCategory("Player 1");
        foreach (var item in DiaryController.diaryItems)
        {
            graphProgress.Data.AddPointToCategory("player 1", ChartDateUtility.DateToValue(item.date.Date), item.subProgress);
        }
        graphProgress.DataSource.EndBatch();

        List<IGrouping<int, MinigameResultItem>> tMinigameCounts = MinigameResultController.minigameResults.GroupBy(item => item.minigame).ToList();
        int tHighestCount = 0;
        int tIndex = 0;
        List<Tuple<int, int>> tIndexeAndCounts = new List<Tuple<int, int>>();
        for (int i = 0; i < tMinigameCounts.Count; i++)
        {
            tIndexeAndCounts.Add(new Tuple<int, int>(tMinigameCounts[i].Key, tMinigameCounts[i].Count()));
            if (i == 0)
            {
                tIndex = i;
                tHighestCount = tMinigameCounts[i].Count();
            }
            else if (tMinigameCounts[i].Count() > tHighestCount)
            {
                tIndex = i;
                tHighestCount = tMinigameCounts[i].Count();
            }
        }
        mostPlayedMinigame.text = GetMinigameNameFromInt(tMinigameCounts[tIndex].Key);
        //TODO use indexes and counts to fill the circle graph if we use it
        diaryEntryCount.text = $"{DiaryController.diaryItems.Count}";
        int highestStar = DiaryController.diaryItems.Max(item => item.smellToday);
        if (highestStar > 1)
        {
            maxStars.gameObject.SetActive(true);
            maxStars.text = $"{highestStar}";
        }
    }

    string GetMinigameNameFromInt(int index)
    {
        switch (index)
        {
            case 0:
                return "Blije neus";
            case 1:
                return "Geur vanger";
            case 2:
                return "Herinnering beschrijven";
            case 3:
                return "Geanimeerde foto";
            case 4:
                return "Simpele timer";
            default:
                return "Geen minigame";
        }
    }
}
