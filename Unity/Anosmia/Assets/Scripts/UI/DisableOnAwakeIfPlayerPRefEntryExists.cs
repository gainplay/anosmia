﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableOnAwakeIfPlayerPRefEntryExists : MonoBehaviour
{
    [SerializeField]
    string _playerprefString;

    private void Awake()
    {
        if (PlayerPrefs.HasKey(_playerprefString))
        {
            gameObject.SetActive(false);
        }
    }

    public void DismissPopup()
    {
        PlayerPrefs.SetInt(_playerprefString,1);
        gameObject.SetActive(false);
    }
}
