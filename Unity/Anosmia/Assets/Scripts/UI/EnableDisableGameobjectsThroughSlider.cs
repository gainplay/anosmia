﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisableGameobjectsThroughSlider : MonoBehaviour
{
    [SerializeField]
    GameObject[] gameObjecteToEnAbleOrDisable;

    public void OnsliderChange(float value)
    {
        foreach (var item in gameObjecteToEnAbleOrDisable)
        {
            item.SetActive(value != 0);
        }
    }
}
