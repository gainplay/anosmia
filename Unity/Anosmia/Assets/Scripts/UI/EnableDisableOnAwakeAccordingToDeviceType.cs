﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableDisableOnAwakeAccordingToDeviceType : MonoBehaviour
{
    [Header("will enable the proper one according to device type and disable the other, in the editor i show the android object")]
    [SerializeField]
    GameObject androidObject;
    [SerializeField]
    GameObject iosObject;

    private void Awake()
    {
        bool androidObjectEnabled;
#if UNITY_ANDROID || UNITY_EDITOR
        androidObjectEnabled = true;
#elif UNITY_IOS
        androidObjectEnabled = false;
#endif

        androidObject.SetActive(androidObjectEnabled);
        iosObject.SetActive(!androidObjectEnabled);
    }
}
