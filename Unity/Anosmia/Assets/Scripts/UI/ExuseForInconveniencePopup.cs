using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExuseForInconveniencePopup : MonoBehaviour
{

#if UNITY_IOS
    private void Start()
    {
        if (GameInitialization.mergedOldANdNewID)
        {
            transform.GetChild(0).gameObject.SetActive(true);
        }
    }
#endif
}
