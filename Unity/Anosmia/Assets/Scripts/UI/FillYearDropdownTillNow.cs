using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using System;

public class FillYearDropdownTillNow : MonoBehaviour
{
    Dropdown dropDown;

    private void Awake()
    {
        dropDown = GetComponent<Dropdown>();
        while (dropDown.options[1].text != DateTime.Now.Year.ToString())
        {
            dropDown.options.Insert(1,new Dropdown.OptionData((int.Parse(dropDown.options[1].text)+1).ToString()));
        }
    }
}
