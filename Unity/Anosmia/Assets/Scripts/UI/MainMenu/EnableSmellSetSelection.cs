﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableSmellSetSelection : MonoBehaviour
{
    [SerializeField]
    GameObject _popupToEnable;
    public void ShouldNewSMellBeSelected()
    {
        DateTime? lastNewSMellDate;
        if (PlayerPrefs.HasKey("newSmellSetDate"))
        {
            lastNewSMellDate = DateTime.Parse(PlayerPrefs.GetString("newSmellSetDate"));
            if ((DateTime.Now - lastNewSMellDate.Value).Days > 90)
            {
                Debug.Log("smellset 2");
                _popupToEnable.SetActive(true);
            }
            else
            {
                _popupToEnable.SetActive(false);
            }
        }
        else
        {
            lastNewSMellDate = null;
            if ((DateTime.Now - SettingsHolder.startDate).Days > 90 && SettingsHolder.startDate != default)
            {
                Debug.Log("smellset 1");
                Debug.Log($"settingholder startdate = {SettingsHolder.startDate}");
                _popupToEnable.SetActive(true);
            }
            else
            {
                Debug.Log("no new smellset yet");
                _popupToEnable.SetActive(false);
            }
        }
    }
}
