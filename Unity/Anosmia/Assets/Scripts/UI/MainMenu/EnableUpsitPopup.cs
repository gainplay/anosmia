﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableUpsitPopup : MonoBehaviour
{

    private int _upsitIndex;
    [SerializeField]
    private GameObject[] _upsitPopups;
    public void Init()
    {
        CheckIfShouldDoUpsit();
    }

    public void CheckIfShouldDoUpsit()
    {
        if (SettingsHolder.upsit0Months == -1)
        {
            _upsitIndex = 0;
            gameObject.SetActive(true);
        }
        else if (SettingsHolder.upsit3Months == -1 && (DateTime.Now - SettingsHolder.startDate).Days > 90)
        {
            _upsitIndex = 1;
            gameObject.SetActive(true);

        }
        else if (SettingsHolder.upsit6Months == -1 && (DateTime.Now - SettingsHolder.startDate).Days > 180)
        {
            _upsitIndex = 2;
            gameObject.SetActive(true);

        }
        else if (SettingsHolder.upsit9Months == -1 && (DateTime.Now - SettingsHolder.startDate).Days > 270)
        {
            _upsitIndex = 3;
            gameObject.SetActive(true);
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    public void DoUpsitClicked()
    {
        _upsitPopups[_upsitIndex].SetActive(true);
    }
}
