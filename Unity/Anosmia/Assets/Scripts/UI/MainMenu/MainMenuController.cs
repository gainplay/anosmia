﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    [SerializeField]
    EnableUpsitPopup upsitPopupController;
    [SerializeField]
    EnableSmellSetSelection smellSetSelectPopupController;
    public bool WHO5Flag = false;
    public GameObject who5Popup;

    private void OnEnable()
    {
        upsitPopupController.Init();
        smellSetSelectPopupController.ShouldNewSMellBeSelected();
        if (WHO5Flag)
        {
            who5Popup.SetActive(true);
        }
    }
}
