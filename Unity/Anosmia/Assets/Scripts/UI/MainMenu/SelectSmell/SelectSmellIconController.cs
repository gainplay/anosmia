﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class SelectSmellIconController : MonoBehaviour
{
    [SerializeField]
    SelectSmellIconHolder iconHolder;

    [SerializeField]
    Image image1, image2, image3, image4;
    [SerializeField]
    TextMeshProUGUI text1, text2, text3, text4;


    private void OnEnable()
    {
        Debug.Log($"Smellset = {SettingsHolder.smellSetSelected}");
        image1.sprite = iconHolder.smellSelectIcons[SettingsHolder.smellSetSelected].sprites[0];
        image2.sprite = iconHolder.smellSelectIcons[SettingsHolder.smellSetSelected].sprites[1];
        image3.sprite = iconHolder.smellSelectIcons[SettingsHolder.smellSetSelected].sprites[2];
        image4.sprite = iconHolder.smellSelectIcons[SettingsHolder.smellSetSelected].sprites[3];
        string[] tNames = SmellSetHelper.GetSMellSet(SettingsHolder.smellSetSelected);
        text1.text = tNames[0];
        text2.text = tNames[1];
        text3.text = tNames[2];
        text4.text = tNames[3];
    }
}
