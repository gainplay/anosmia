﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "SelectSmellIconHolderScriptable", menuName = "Scriptable/SmellSelectIconHolder")]
public class SelectSmellIconHolder : ScriptableObject
{
    public SmellSelectIcon[] smellSelectIcons;

    [System.Serializable]
    public struct SmellSelectIcon
    {
        public Sprite[] sprites;
    }
}
