﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using TMPro;
using System;

public class TrainingCountController : MonoBehaviour
{
    [SerializeField]
    private bool _isMorningCount;
    [SerializeField]
    private ScriptableDateTime _eveningTime;
    [SerializeField]
    TextMeshProUGUI countText;
    [SerializeField]
    TrainingCounterIcons icons;
    [SerializeField]
    Image[] images, imageBackgrounds;

    public void OnEnable()
    {
        MinigameResultItem[] tItems = (from item in MinigameResultController.minigameResults where item.trainingTime.Date == DateTime.Now.Date select item).ToArray();
        if (_isMorningCount)
        {
            tItems = (from item in tItems where item.trainingTime.Hour < _eveningTime.dateTime.Hour orderby item.trainingTime ascending select item).ToArray();
        }
        else
        {
            tItems = (from item in tItems where item.trainingTime.Hour >= _eveningTime.dateTime.Hour orderby item.trainingTime ascending select item).ToArray();
        }
        if (tItems.Length >= 4)
        {
            countText.text = "4/4";
            for (int i = 0; i < 4; i++)
            {
                images[i].color = Color.white;
                images[i].sprite = icons.GetIcon(tItems[i].smell, tItems[i].smellSet);
                imageBackgrounds[i].color = icons.GetColor(tItems[i].smell, tItems[i].smellSet);
            }
        }
        else
        {
            countText.text = $"{tItems.Length}/4";
            for (int i = 0; i < 4; i++)
            {
                images[i].color = Color.clear;
                imageBackgrounds[i].color = Color.clear;
            }
            for (int i = 0; i < tItems.Length; i++)
            {
                images[i].color = Color.white;
                images[i].sprite = icons.GetIcon(tItems[i].smell, tItems[i].smellSet);
                imageBackgrounds[i].color = icons.GetColor(tItems[i].smell, tItems[i].smellSet);

            }
        }
    }

}
