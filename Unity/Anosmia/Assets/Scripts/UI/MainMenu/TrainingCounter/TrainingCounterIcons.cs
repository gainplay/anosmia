﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Scriptable/TraningCounterIcons")]
public class TrainingCounterIcons : ScriptableObject
{
    public List<iconItem> icons;

    public Sprite GetIcon(int smell, int smellSet)
    {
        return icons.Find(t => t.smell == smell && t.smellSet == smellSet).sprite;
    }

    public Color GetColor(int smell, int smellSet)
    {
        return icons.Find(t => t.smell == smell && t.smellSet == smellSet).color;
    }


    [System.Serializable]
    public struct iconItem { public Sprite sprite; public int smell; public int smellSet; public Color color; }
}
