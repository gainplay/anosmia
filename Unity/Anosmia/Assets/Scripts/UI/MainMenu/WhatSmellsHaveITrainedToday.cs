﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class WhatSmellsHaveITrainedToday : MonoBehaviour
{
    [SerializeField]
    private WhatSmellsHaveITrainedTodayItem[] smellItemsMorning;
    [SerializeField]
    private WhatSmellsHaveITrainedTodayItem[] smellItemsEvening;


    public void OnEnable()
    {

        Refresh();
        
    }

    public void Refresh()
    {
        List<MinigameResultItem> tTrainingsOfToday = MinigameResultController.minigameResults.FindAll(l => l.trainingTime.Date == DateTime.Today && l.smellSet == SettingsHolder.smellSetSelected).OrderByDescending(l => l.trainingTime).ToList();
        ArrangeAndSetTexts(smellItemsMorning, tTrainingsOfToday.FindAll(l => l.trainingTime.Hour < 17));
        ArrangeAndSetTexts(smellItemsEvening, tTrainingsOfToday.FindAll(l => l.trainingTime.Hour >= 17));
    }

    private void ArrangeAndSetTexts(WhatSmellsHaveITrainedTodayItem[] listToArrange, List<MinigameResultItem> dataToArrangeThemBy)
    {
        string[] currentSmells = SmellSetHelper.GetSMellSet(SettingsHolder.smellSetSelected);
        int tTrainingCount0 = dataToArrangeThemBy.Where(l => l.smell == 1).Count();
        int tTrainingCount1 = dataToArrangeThemBy.Where(l => l.smell == 2).Count();
        int tTrainingCount2 = dataToArrangeThemBy.Where(l => l.smell == 3).Count();
        int tTrainingCount3 = dataToArrangeThemBy.Where(l => l.smell == 4).Count();

        listToArrange[0].Init(currentSmells[0], tTrainingCount0);
        listToArrange[1].Init(currentSmells[1], tTrainingCount1);
        listToArrange[2].Init(currentSmells[2], tTrainingCount2);
        listToArrange[3].Init(currentSmells[3], tTrainingCount3);


        List<int> tIndexes = dataToArrangeThemBy.Select(l=> l.smell).Distinct().ToList();
        //we loop though the distinct values and set them as first sibling
        //because we sorted them to be most recent first that will be the one on the bottom of the enabled ones
        //for example kruidnagel, roos,  citroen, eucalyptus is the order we trained in
        //order at start
        //roos, eucalyptus, kruidnagel, citroen
        //iteration 1
        //eucalyptus, roos, kruidnagel, citroen  
        //iteration 2
        //citroen, eucalyptus, roos, kruidnagel
        //iteration 3
        //roos, citroen, eucalyptus, kruidnagel
        //iteration 4
        //kruidnagel, roos, citroen, eucalyptus
        //as you can see it ends up in the right order because the last one is sorted to the front first
        //and the most recent one is set to the front in the last iteration
        for (int i = 0; i < tIndexes.Count; i++)
        {
            //the -1 is because 0 is no smell in the minigame list and smell '0' is 1
            listToArrange[tIndexes[i] -1].transform.SetAsFirstSibling();
        }
    }
}
