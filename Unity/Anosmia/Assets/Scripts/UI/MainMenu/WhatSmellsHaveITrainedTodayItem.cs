﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

[System.Serializable]
public class WhatSmellsHaveITrainedTodayItem : MonoBehaviour
{
    [SerializeField]
    Toggle myToggle;
    [SerializeField]
    TextMeshProUGUI smellText;
    [SerializeField]
    TextMeshProUGUI smellAmountText;


    public void Init(string smellName, int smellAmount)
    {
        myToggle.isOn = smellAmount > 0;
        smellText.text = smellName;
        smellAmountText.text = smellAmount > 0 ? $"x{smellAmount}" : "";
    }
}
