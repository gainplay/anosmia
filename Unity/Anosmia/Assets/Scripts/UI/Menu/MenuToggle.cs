﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MenuToggle : MonoBehaviour
{
    [SerializeField]
    GameObject _toggleOn;
    [SerializeField]
    GameObject _toggleOff;

    
    [Tooltip("These objects are enabled if the toggle.isOn == true")]
    [SerializeField][FormerlySerializedAs("_objectsToToggle")]
    GameObject[] _objectsToToggle;



    public void OnToggle(bool isOn)
    {
        _toggleOff.SetActive(!isOn);
        _toggleOn.SetActive(isOn);

        foreach (var item in _objectsToToggle)
        {
            item.SetActive(isOn);
        }
    }
}
