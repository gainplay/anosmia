﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
using UnityEngine.Networking;
using TMPro;

public class DiaryToday : MonoBehaviour
{
    [SerializeField]
    Slider journeyProgressSLider;
    int smellToday = -1;
    [SerializeField]
    Toggle[] smellTodayToggles;
    [SerializeField]
    TMP_InputField diaryEntry;
    DateTime currentDate;
    [SerializeField]
    ToggleMuter _toggleMuter;
    [SerializeField]
    TextMeshProUGUI _dateText;
    [SerializeField]
    GameObject[] starTexts;

    /// <summary>
    /// opens the diary with the selected date
    /// </summary>
    /// <param name="targetDate">the selected date</param>
    public void Init(DateTime targetDate)
    {
        DiaryItem tItem = DiaryController.diaryItems.FirstOrDefault(l => l.date.Date == targetDate.Date);
        if (tItem != null)
        {
            journeyProgressSLider.value = tItem.subProgress;
            smellToday = tItem.smellToday;
            EnableStarsTillIndex();
            diaryEntry.text = tItem.text;
        }
        else
        {
            journeyProgressSLider.value = 0;
            smellToday = -1;
            EnableStarsTillIndex();
            diaryEntry.text = "";
        }
        currentDate = targetDate;
        _dateText.text = currentDate.ToString("dd-MM-yyyy");
        gameObject.SetActive(true);
        Debug.Log($"targetDate = {targetDate}");
    }

    void EnableStarsTillIndex()
    {
        _toggleMuter.Mute();
        starTexts[0].SetActive(false);
        for (int i = 0; i < smellTodayToggles.Length; i++)
        {
            if (i <= smellToday)
            {
                smellTodayToggles[i].isOn = true;
                
            }
            else
            {
                smellTodayToggles[i].isOn = false;
            }
            //we toggle i+1 because index 0 is no stars clicked
            //we use i == smelltoday because i will loop though all of the toggles and not all of the starTexts 
            starTexts[i + 1].SetActive(i == smellToday);
        }
        _toggleMuter.UnMute();
    }

    /// <summary>
    /// opens the diary with the date of today
    /// </summary>
    public void Init() => Init(DateTime.Today);

    public void SetSmellToday(int value)
    {
        smellToday = value;
        EnableStarsTillIndex();
    }

    public void SaveClicked()
    {
        DiaryItem tItem = DiaryController.diaryItems.FirstOrDefault(l => l.date.Date == currentDate.Date);
        Debug.Log($"tItem date = {tItem?.date }, currentDate = {currentDate}");
        if (tItem == null)
        {
            tItem = new DiaryItem(currentDate, diaryEntry.text, (int)journeyProgressSLider.value, smellToday);
            DiaryController.diaryItems.Add(tItem);
            TestKalendar.Refresh?.Invoke();
        }
        else
        {
            tItem.smellToday = smellToday;
            tItem.subProgress = (int)journeyProgressSLider.value;
            tItem.text = diaryEntry.text;
        }
        StartCoroutine(SaveTraningExperienceToDB());
    }

    private IEnumerator SaveTraningExperienceToDB()
    {
        WWWForm tForm = new WWWForm();
        tForm.AddField("ENTRY", diaryEntry.text);
        UnityWebRequest tRequest = UnityWebRequest.Post(DatabaseHelper.baseUrl + "insertOrUpdateDiary.php",tForm);
        tRequest.SetRequestHeader("SMELLTODAY", smellToday.ToString());
        tRequest.SetRequestHeader("SUBPROGRESS", Mathf.RoundToInt(journeyProgressSLider.value).ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        tRequest.SetRequestHeader("DATE", currentDate.ToString("yyyy-MM-dd"));
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        yield return new WaitUntil(() => tRequest.isDone);
        gameObject.SetActive(false);
    }
}
