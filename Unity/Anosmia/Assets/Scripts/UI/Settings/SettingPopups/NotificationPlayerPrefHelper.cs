﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class NotificationPlayerPrefHelper
{
    public const string morningTimeName = "notificationMorning";
    public const string eveningTimeName = "notificationEvening";
    public const string notificationsEnabledName = "enabledNotifications";

    public static bool HasMorningKey => PlayerPrefs.HasKey(morningTimeName);


    public static bool HasEveningKey => PlayerPrefs.HasKey(eveningTimeName);


    public static bool HasEnabledKey => PlayerPrefs.HasKey(notificationsEnabledName);


    public static string MorningTime
    {
        get
        {
            return PlayerPrefs.GetString(morningTimeName);
        }
        set
        {
            PlayerPrefs.SetString(morningTimeName, value);
        }
    }
    public static string EveningTime
    {
        get
        {
            return PlayerPrefs.GetString(eveningTimeName);
        }
        set
        {
            PlayerPrefs.SetString(eveningTimeName, value);
        }
    }
    public static bool NotificationsEnabled
    {
        get
        {
            return PlayerPrefs.HasKey(notificationsEnabledName) ? PlayerPrefs.GetInt(notificationsEnabledName) != 0 : true;
        }
        set
        {
            PlayerPrefs.SetInt(notificationsEnabledName, value ? 1 : 0);
        }
    }
}
