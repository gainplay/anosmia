﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PersonalGoalController : MonoBehaviour
{
    [SerializeField]
    TMPro.TMP_InputField _inputField;

    private IEnumerator Start()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "getGoal.php");
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        if (string.IsNullOrEmpty(tRequest.error))
        {
            _inputField.text = tRequest.downloadHandler.text;
        }
    }

    public void SavePersonalGoal()
    {
        StartCoroutine(SendPersonalGoal());
    }

    IEnumerator SendPersonalGoal()
    {
        WWWForm tForm = new WWWForm();
        tForm.AddField("GOAL", _inputField.text);
        UnityWebRequest tRequest = UnityWebRequest.Post(DatabaseHelper.baseUrl + "sendGoal.php", tForm);
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        tRequest.SendWebRequest();
        yield return new WaitUntil(() => tRequest.isDone);
        if (string.IsNullOrEmpty(tRequest.error))
        {
            Debug.Log(tRequest.downloadHandler.text);
        }
        else
        {
            Debug.Log(tRequest.error);
        }
        gameObject.SetActive(false);
    }
}
