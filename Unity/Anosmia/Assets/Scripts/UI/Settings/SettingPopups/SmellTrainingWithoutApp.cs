﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SmellTrainingWithoutApp : MonoBehaviour
{
    /// <summary>
    /// 0-3 will be morning smell and 4-7 will be evening smells
    /// </summary>
    private List<int> selectedOptions = new List<int>();


    [SerializeField]
    private SettingsPageController _settingsPageController;

    [SerializeField]
    private List<GameObject> set1 = new List<GameObject>();
    [SerializeField]
    private List<GameObject> set2 = new List<GameObject>();
    [SerializeField]
    private List<GameObject> set3 = new List<GameObject>();
    [SerializeField]
    private List<Toggle> toggles = new List<Toggle>();
    [SerializeField]
    DiaryToday diary;
    [SerializeField]
    WhatSmellsHaveITrainedToday whatSmellHaveITrainedToday;
    [SerializeField]
    TrainingCountController _trainingCountControllerMorning;
    [SerializeField]
    TrainingCountController _trainingCountControllerEvening;

    IEnumerator saveRoutine;

    private void OnEnable()
    {
        saveRoutine = null;
        foreach (var item in toggles)
        {
            item.isOn = false;
        }
        for (int i = 0; i < set1.Count; i++)
        {
            set1[i].SetActive(SettingsHolder.smellSetSelected == 0);
            set2[i].SetActive(SettingsHolder.smellSetSelected == 1);
            set3[i].SetActive(SettingsHolder.smellSetSelected == 2);
        }
    }

    public void AddOption(int option)
    {
        selectedOptions.Add(option);
    }
    public void RemoveOption(int option)
    {
        selectedOptions.Remove(option);
    }

    public void SaveClicked()
    {
        if (saveRoutine == null)
        {
            saveRoutine = SaveSMellTrainingWithoutApp(false);
            StartCoroutine(saveRoutine);
        }
    }

    public void SaveClickedWithDiaryButton()
    {
        if (saveRoutine == null)
        {
            saveRoutine = SaveSMellTrainingWithoutApp(true);
            StartCoroutine(saveRoutine);
        }
    }

    private IEnumerator SaveSMellTrainingWithoutApp(bool openDiary)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "sendTrainingWithoutMinigame.php");
        tRequest.SetRequestHeader("SELECTEDOPTIONS", string.Join(",", selectedOptions.Distinct()));
        tRequest.SetRequestHeader("CURRENTSET", SettingsHolder.smellSetSelected.ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateSmellTrainingExperienceText();
        yield return MinigameResultController.LoadMinigameResults();
        yield return new WaitForEndOfFrame();
        whatSmellHaveITrainedToday.Refresh();
        _trainingCountControllerMorning.OnEnable();
        _trainingCountControllerEvening.OnEnable();
        if (openDiary)
        {
            diary.Init();
        }
        saveRoutine = null;
        gameObject.SetActive(false);
    }
}
