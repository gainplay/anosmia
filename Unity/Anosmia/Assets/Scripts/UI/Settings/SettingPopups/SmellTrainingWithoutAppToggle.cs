﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmellTrainingWithoutAppToggle : MonoBehaviour
{
    [SerializeField]
    private SmellTrainingWithoutApp controller;
    [SerializeField]
    private int smellIndex;
    public void OnToggle(bool value)
    {
        if (value)
        {
            controller.AddOption(smellIndex);
        }
        else
        {
            controller.RemoveOption(smellIndex);

        }
    }
}
