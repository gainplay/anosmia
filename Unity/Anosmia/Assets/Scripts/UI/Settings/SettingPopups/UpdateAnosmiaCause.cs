﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UpdateAnosmiaCause : MonoBehaviour
{
    List<int> selectedCauses = new List<int>();

    [SerializeField]
    SettingsPageController _settingsPageController;
    [SerializeField]
    Toggle diagnosedByDoctorToggle;
    [SerializeField]
    Toggle diagnosedBySelfToggle;
    [SerializeField]
    List<UpdateAnosmiaCauseToggle> causeTogglesScripts = new List<UpdateAnosmiaCauseToggle>();

    private void OnEnable()
    {
        string[] tCausesSeperated = SettingsHolder.causeOfAnosmia.Split(',');
        foreach (var item in causeTogglesScripts)
        {
            item.myToggle.isOn = false;
        }
        for (int i = 0; i < tCausesSeperated.Length; i++)
        {
            causeTogglesScripts[int.Parse(tCausesSeperated[i])].myToggle.isOn = true;
        }
        StartCoroutine(WaitForEndOfFrameThenPrefomFunction.WaitForEndOfFrameThenAction(SetTogglegroupToggles));
    }

    private void SetTogglegroupToggles()
    {
        diagnosedByDoctorToggle.isOn = SettingsHolder.diagnosedByDoctor;
        diagnosedBySelfToggle.isOn = !SettingsHolder.diagnosedByDoctor;
    }

    public void AddInt(int value)
    {
        selectedCauses.Add(value);
    }

    public void RemoveInt(int value)
    {
        selectedCauses.Remove(value);
    }


    private string CausesToDBString()
    {
        string tResult = "";
        foreach (var item in selectedCauses)
        {
            tResult += $"{item},";
        }
        tResult = tResult.TrimEnd(',');
        return tResult;
    }

    public void SaveClicked()
    {
        SettingsHolder.causeOfAnosmia = CausesToDBString();
        SettingsHolder.diagnosedByDoctor = diagnosedByDoctorToggle.isOn;
        StartCoroutine(UpdateCauseAndDiagnosedByDoctor());
    }

    IEnumerator UpdateCauseAndDiagnosedByDoctor()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateCauseAndDiagnosedByDoctor.php");
        tRequest.SetRequestHeader("CAUSE", CausesToDBString());
        tRequest.SetRequestHeader("DIAGNOSEDBYDOCTOR", (diagnosedByDoctorToggle.isOn ? 1 : 0).ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateCauseOfAnosmiaText();
        gameObject.SetActive(false);
    }
}
