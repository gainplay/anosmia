﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateAnosmiaCauseToggle : MonoBehaviour
{
    [SerializeField]
    UpdateAnosmiaCause controller;
    public int index;
    public Toggle myToggle;


    private void Awake()
    {
        myToggle = GetComponent<Toggle>();
    }


    public void OnToggle(bool value)
    {
        if (value)
        {
            controller.AddInt(index);
        }
        else
        {
            controller.RemoveInt(index);
        }
    }
}
