﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UpdateAnosmiaFrom : MonoBehaviour
{
    [SerializeField]
    Toggle enabledToggle;
    [SerializeField]
    Toggle dontKnowToggle;
    [SerializeField]
    Dropdown _sinceWhenMonthDropdown;
    [SerializeField]
    Dropdown _sinceWhenYearDropDown;


    [SerializeField]
    SettingsPageController _settingsPageController;

    private void OnEnable()
    {
        StartCoroutine(WaitForEndOfFrameThenPrefomFunction.WaitForEndOfFrameThenAction(Init));
    }

    void Init()
    {
        if (SettingsHolder.anosmiaFrom != default)
        {
            enabledToggle.isOn = true;
            _sinceWhenMonthDropdown.value = SettingsHolder.anosmiaFrom.Month;
            string yearString = SettingsHolder.anosmiaFrom.ToString("yyyy");
            for (int i = 0; i < _sinceWhenYearDropDown.options.Count; i++)
            {
                if (_sinceWhenYearDropDown.options[i].text == yearString)
                {

                    _sinceWhenYearDropDown.value = i;
                    break;
                }
            }
        }
        else
        {
            dontKnowToggle.isOn = true;
        }
    }


    public void SaveClicked()
    {
        StartCoroutine(SaveTrainingExperienceToDB());
        SettingsHolder.anosmiaFrom = enabledToggle.isOn ? DateTime.ParseExact($"{_sinceWhenYearDropDown.ValueText()}-{_sinceWhenMonthDropdown.ValueText()}", "yyyy-MMMM", new CultureInfo("nl-NL")) : default;
    }

    IEnumerator SaveTrainingExperienceToDB()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateAnosmiaFrom.php");
        tRequest.SetRequestHeader("ANOSMIAFROM", enabledToggle.isOn ? AnosmiaFromDateDropdownsToDBString() : "null");
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateAnosmiaSinceText();
        gameObject.SetActive(false);
    }


    private string AnosmiaFromDateDropdownsToDBString()
    {
        Debug.Log($"input = {$"{_sinceWhenYearDropDown.ValueText()}-{_sinceWhenMonthDropdown.ValueText()}"}, output = {DateTime.ParseExact($"{_sinceWhenYearDropDown.ValueText()}-{_sinceWhenMonthDropdown.ValueText()}", "yyyy-MMMM", new CultureInfo("nl-NL")).ToString("yyyy-MM")}");
        return DateTime.ParseExact($"{_sinceWhenYearDropDown.ValueText()}-{_sinceWhenMonthDropdown.ValueText()}", "yyyy-MMMM", new CultureInfo("nl-NL")).ToString("yyyy-MM-dd");
    }
}
