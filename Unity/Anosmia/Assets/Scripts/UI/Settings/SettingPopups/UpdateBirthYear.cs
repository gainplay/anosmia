﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UpdateBirthYear : MonoBehaviour
{
    [SerializeField]
    Toggle _birthYearEnabledToggle;
    [SerializeField]
    Toggle _birthYearDisabledToggle;
    [SerializeField]
    Dropdown _birthYearDropDown;
    [SerializeField]
    SettingsPageController _settingsPageController;
    [SerializeField]
    Button saveButton;

    private void OnEnable()
    {
        if (SettingsHolder.birthYear <= 0)
        {
            _birthYearDisabledToggle.isOn = true;
        }
        else
        {
            _birthYearEnabledToggle.isOn = true;
            string birthYearString = SettingsHolder.birthYear.ToString();
            for (int i = 0; i < _birthYearDropDown.options.Count; i++)
            {
                if (_birthYearDropDown.options[i].text == birthYearString)
                {
                    _birthYearDropDown.value = i;
                    break;
                }
            }
        }
    }

    public void OnValueChanged()
    {
        if (_birthYearDisabledToggle.isOn || _birthYearEnabledToggle.isOn && _birthYearDropDown.value != 0)
        {
            saveButton.interactable = true;
        }
        else
        {
            saveButton.interactable = false;
        }
    }


    public void SaveClicked()
    {
        int tSelectedYear = _birthYearEnabledToggle.isOn && _birthYearDropDown.value != 0 ? int.Parse(_birthYearDropDown.options[_birthYearDropDown.value].text) : -2;
        if (SettingsHolder.birthYear != tSelectedYear)
        {
            SettingsHolder.birthYear = tSelectedYear;
            StartCoroutine(UpdateBirthYearInDB());
        }
    }

    IEnumerator UpdateBirthYearInDB()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateBirthYear.php");
        tRequest.SetRequestHeader("BIRTHYEAR", (_birthYearEnabledToggle.isOn && _birthYearDropDown.value != 0 ? int.Parse(_birthYearDropDown.options[_birthYearDropDown.value].text) : -2).ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateBirthYearText();
        gameObject.SetActive(false);
    }
}
