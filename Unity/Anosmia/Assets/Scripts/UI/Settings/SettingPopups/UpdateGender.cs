﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UpdateGender : MonoBehaviour
{
    int selectedGender;
    [SerializeField]
    SettingsPageController _settingsPageController;
    [Header("0 vertelIkLieverNiet, 1 male, 2 female, 3 other")]
    [SerializeField]
    Toggle[] toggles;

    private void OnEnable()
    {
        StartCoroutine(WaitAFrame());
    }

    IEnumerator WaitAFrame()
    {
        yield return new WaitForEndOfFrame();
        toggles[SettingsHolder.gender].isOn = true;
    }

    public void SelectGender(int value)
    {
        selectedGender = value;
    }

    public void SaveClicked()
    {
        if (selectedGender != SettingsHolder.gender)
        {
            SettingsHolder.gender = selectedGender;
            StartCoroutine(UpdateGenderInDB());
        }
        else
        {
            gameObject.SetActive(false);
        }
    }

    IEnumerator UpdateGenderInDB()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateGender.php");
        tRequest.SetRequestHeader("GENDER", selectedGender.ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateGenderText();
        gameObject.SetActive(false);
    }
}
