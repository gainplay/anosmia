﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;

public class UpdateNotifications : MonoBehaviour
{
    [SerializeField]
    MyNotificationManager notificationsManager;

    //android
    [SerializeField]
    Text morningTimeText;
    [SerializeField]
    Text eveningTimeText;
    //ios
    [SerializeField]
    Dropdown morningHours;
    [SerializeField]
    Dropdown morningMinutes;
    [SerializeField]
    Dropdown eveningHours;
    [SerializeField]
    Dropdown eveningMinutes;


    private string selectedEveningTime;
    private string selectedMorningTime;
    public InputField morningRoutine;
    public InputField eveningRoutine;
    [SerializeField]
    Slider _notificationsEnabledSlider;


    [SerializeField]
    SettingsPageController _settingsPageController;

    private void OnEnable()
    {
        _notificationsEnabledSlider.value = NotificationPlayerPrefHelper.NotificationsEnabled ? 1 : 0;
        morningRoutine.text = SettingsHolder.notificationMorningRoutine;
        eveningRoutine.text = SettingsHolder.notificationEveningRoutine;
#if UNITY_ANDROID
        morningTimeText.text = selectedMorningTime = NotificationPlayerPrefHelper.HasMorningKey ? (!string.IsNullOrEmpty(NotificationPlayerPrefHelper.MorningTime) ? NotificationPlayerPrefHelper.MorningTime : "06:00") : "06:00";
        eveningTimeText.text = selectedEveningTime = NotificationPlayerPrefHelper.HasEveningKey ? (!string.IsNullOrEmpty(NotificationPlayerPrefHelper.EveningTime) ? NotificationPlayerPrefHelper.EveningTime : "19:00") : "19:00"; 
#elif UNITY_IOS
        string[] morningSplit = (NotificationPlayerPrefHelper.HasMorningKey ? (!string.IsNullOrEmpty(NotificationPlayerPrefHelper.MorningTime) ? NotificationPlayerPrefHelper.MorningTime : "06:00") : "06:00").Split(':');
        string[] eveningSplit = (NotificationPlayerPrefHelper.HasEveningKey ? (!string.IsNullOrEmpty(NotificationPlayerPrefHelper.EveningTime) ? NotificationPlayerPrefHelper.EveningTime : "19:00") : "19:00").Split(':');

        foreach (var item in morningHours.options)
        {
            if (item.text == morningSplit[0])
            {
                morningHours.value = morningHours.options.IndexOf(item);
            }
        }
        foreach (var item in morningMinutes.options)
        {
            if (item.text == morningSplit[1])
            {
                morningMinutes.value = morningMinutes.options.IndexOf(item);
            }
        }

        foreach (var item in eveningHours.options)
        {
            if (item.text == eveningSplit[0])
            {
                eveningHours.value = eveningHours.options.IndexOf(item);
            }
        }
        foreach (var item in eveningMinutes.options)
        {
            if (item.text == eveningSplit[1])
            {
                eveningMinutes.value = eveningMinutes.options.IndexOf(item);
            }
        }

#endif
    }

    public void MorningTimeChanged(string hoursAndMinutes)
    {
        Debug.Log($"android: morning notification time {hoursAndMinutes}");
        selectedMorningTime = hoursAndMinutes;
    }

    public void EveningTimeChanged(string hoursAndMinutes)
    {
        Debug.Log($"android: evening notification time {hoursAndMinutes}");

        selectedEveningTime = hoursAndMinutes;
    }

    public void MorningDropdownValueChanged()
    {
        selectedMorningTime = $"{morningHours.ValueText()}:{morningMinutes.ValueText()}";
    }

    public void EveningDropdownValueChanged()
    {
        selectedEveningTime = $"{eveningHours.ValueText()}:{eveningMinutes.ValueText()}";
    }

    public void SaveClicked()
    {
        Debug.Log($"android: start of save clicked");

        if (_notificationsEnabledSlider.value == 1)
        {
            StartCoroutine(SaveRoutine());
            NotificationPlayerPrefHelper.NotificationsEnabled = true;

        }
        else
        {
            notificationsManager.CancelAllNotifications();
            NotificationPlayerPrefHelper.NotificationsEnabled = false;
            gameObject.SetActive(false);
        }

        _settingsPageController.UpdateNotificationText();
        _settingsPageController.UpdayeNotificationSlider();

        Debug.Log($"android: end of save clicked");

    }

    IEnumerator SaveRoutine()
    {
        NotificationPlayerPrefHelper.MorningTime = selectedMorningTime;
        NotificationPlayerPrefHelper.EveningTime = selectedEveningTime;
        notificationsManager.CancelAllNotifications();
        notificationsManager.ScheduleNotifications();

        SettingsHolder.notificationMorningRoutine = morningRoutine.text;
        SettingsHolder.notificationEveningRoutine = eveningRoutine.text;

        WWWForm tForm = new WWWForm();
        tForm.AddField("MORNINGROUTINE", morningRoutine.text);
        tForm.AddField("EVENINGROUTINE", eveningRoutine.text);
        UnityWebRequest tRequest = UnityWebRequest.Post(DatabaseHelper.baseUrl + "updateRoutines.php",tForm);
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateNotificationText();
        gameObject.SetActive(false);
    }
}
