﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UpdateSmellSenseDescription : MonoBehaviour
{
    int _selecedSmellSense;

    [SerializeField]
    SettingsPageController _settingsPageController;

    [SerializeField]
    Toggle[] toggles;

    private void OnEnable()
    {
        StartCoroutine(WaitForEndOfFrameThenPrefomFunction.WaitForEndOfFrameThenAction(() => toggles[SettingsHolder.smellAbility].isOn = true));
    }

    public void SetSelectedSmellSence(int smellSense)
    {
        _selecedSmellSense = smellSense;
    }

    public void SaveClicked()
    {
        SettingsHolder.smellAbility = _selecedSmellSense;
        StartCoroutine(SaveTraningExperienceToDB());
    }

    private IEnumerator SaveTraningExperienceToDB()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateSmellSense.php");
        tRequest.SetRequestHeader("SMELLSENSE", _selecedSmellSense.ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateDescriptionLessenedSmell();
        gameObject.SetActive(false);
    }
}
