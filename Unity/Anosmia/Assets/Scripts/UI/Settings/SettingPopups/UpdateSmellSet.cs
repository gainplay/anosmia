﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UpdateSmellSet : MonoBehaviour
{

    [SerializeField]
    SettingsPageController _settingsPageController;
    [SerializeField]
    SmellSetItem[] smellSetPanels;

   

    public void SaveClicked()
    {
        int tSMellSetSelected = 1;
        Vector2 tClosestRectCenter = new Vector2(int.MaxValue,int.MaxValue);
        foreach (var item in smellSetPanels)
        {
            Debug.Log($"item name = {item.name}, position = {item.myRecttransform.anchoredPosition}, index = {item.setIndex}");
            Debug.Log($"bool result {Mathf.Abs(item.myRecttransform.anchoredPosition.x) < tClosestRectCenter.x}, anchorPos = {item.myRecttransform.anchoredPosition}, closest center = {tClosestRectCenter}");
            if (Mathf.Abs(item.myRecttransform.anchoredPosition.x) < Mathf.Abs(tClosestRectCenter.x))
            {
                tClosestRectCenter = item.myRecttransform.anchoredPosition;
                tSMellSetSelected = item.setIndex;
            }
        }
        Debug.Log(tSMellSetSelected);
        PlayerPrefs.SetString("newSmellSetDate", DateTime.Now.Date.ToString());
        SettingsHolder.smellSetSelected = tSMellSetSelected;
        StartCoroutine(SaveTraningExperienceToDB(tSMellSetSelected));
    }

    private IEnumerator SaveTraningExperienceToDB(int selectedSmellSet)
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateSmellSet.php");
        tRequest.SetRequestHeader("SMELLSET", selectedSmellSet.ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateDescriptionLessenedSmell();
        _settingsPageController.HandleSmellSet();
        gameObject.SetActive(false);
    }
}
