﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UpdateTrainingExperience : MonoBehaviour
{
    private int _selectedOption = -1;

    [SerializeField]
    SettingsPageController _settingsPageController;
    [SerializeField]
    Toggle[] toggles;

    private void OnEnable()
    {
        StartCoroutine(WaitForEndOfFrameThenPrefomFunction.WaitForEndOfFrameThenAction(SetToggleGroupToggles));
    }

    void SetToggleGroupToggles()
    {
        toggles[SettingsHolder.anosmiaTrainingExperience].isOn = true;
    }

    public void SetSelectedOption(int option)
    {
        _selectedOption = option;
    }

    public void SaveClicked()
    {
        SettingsHolder.anosmiaTrainingExperience = _selectedOption;
        StartCoroutine(SaveTraningExperienceToDB());
    }

    private IEnumerator SaveTraningExperienceToDB()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateTrainingExperience.php");
        tRequest.SetRequestHeader("TRAININGEXPERIENCE", _selectedOption.ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateSmellTrainingExperienceText();
        gameObject.SetActive(false);
    }
}
