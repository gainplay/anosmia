﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UpdateUpsit : MonoBehaviour
{
    [SerializeField]
    Slider inputField;
    [SerializeField]
    int upsitTimeInMonths;


    [SerializeField]
    private SettingsPageController _settingsPageController;

    public void SaveClicked()
    {
        switch (upsitTimeInMonths)
        {
            case 0:
                SettingsHolder.upsit0Months = Mathf.RoundToInt(inputField.value);
                break;
            case 3:
                SettingsHolder.upsit3Months = Mathf.RoundToInt(inputField.value);
                break;
            case 6:
                SettingsHolder.upsit6Months = Mathf.RoundToInt(inputField.value);
                break;
            case 9:
                SettingsHolder.upsit9Months = Mathf.RoundToInt(inputField.value);
                break;
            default:
                Debug.Log("invalid month count");
                break;
        }
        StartCoroutine(SaveTraningExperienceToDB());
    }

    private IEnumerator SaveTraningExperienceToDB()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateUpsit.php");
        tRequest.SetRequestHeader("SCORE", Mathf.RoundToInt(inputField.value).ToString());
        tRequest.SetRequestHeader("MONTHS", upsitTimeInMonths.ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _settingsPageController.UpdateUpsitButtons();
        gameObject.SetActive(false);
    }
}
