﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Who5Enquete : MonoBehaviour
{
    private int _currentWHO5 = -1;
    [SerializeField]
    Who5EnqueteItem[] _sections;
    [SerializeField]
    GameObject startScreen;
    [SerializeField]
    Who5EnqueteEndScreen _resultScreen;
    [SerializeField]
    SettingsPageController _settingsPageController;

    public int EndScore { get { return SumOfSections() * 4; } }

    private void OnEnable()
    {
        startScreen.SetActive(true);
        _resultScreen.gameObject.SetActive(false);
        for (int i = 0; i < _sections.Length; i++)
        {
            _sections[i].gameObject.SetActive(false);
        }
    }

    public void StartEnquete(int who5Index)
    {
        _currentWHO5 = who5Index;
        gameObject.SetActive(true);
    }

    public void SaveClicked()
    {
        if (_currentWHO5 == 0)
        {
            SettingsHolder.who5_0months = EndScore;
        }
        else if (_currentWHO5 == 1)
        {
            SettingsHolder.who5_3months = EndScore;
        }
        else if (_currentWHO5 == 2)
        {
            SettingsHolder.who5_6months = EndScore;

        }
        else if (_currentWHO5 == 3)
        {
            SettingsHolder.who5_9months = EndScore;

        }
        StartCoroutine(SaveWHO5ToDB());
    }

    private IEnumerator SaveWHO5ToDB()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateWHO5.php");
        tRequest.SetRequestHeader("WHOSCORE", EndScore.ToString());
        tRequest.SetRequestHeader("WHOINDEX", _currentWHO5.ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        gameObject.SetActive(false);
    }

    private int SumOfSections()
    {
        int tResult = 0;
        foreach (var item in _sections)
        {
            tResult += item.score;
        }
        return tResult;
    }
}
