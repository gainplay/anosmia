﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Who5EnqueteEndScreen : MonoBehaviour
{
    [SerializeField]
    Who5Enquete _controller;
    [SerializeField]
    TextMeshProUGUI _scoreText;
    private void OnEnable()
    {
        _scoreText.text = _controller.EndScore.ToString();
    }
}
