﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Who5NotificationPopup : MonoBehaviour
{
    [SerializeField]
    Who5Enquete _enqueteController;
    int _who5Index;


    [SerializeField]
    SettingsPageController _settingsPageController;
    [SerializeField]
    MainMenuController _mainMenuController;

    private void OnEnable()
    {
        if (SettingsHolder.who5_9months == -1 && SettingsHolder.startDate + TimeSpan.FromDays(270) < DateTime.Now)
        {
            _who5Index = 3;
        }
        else if (SettingsHolder.who5_6months == -1 && SettingsHolder.startDate + TimeSpan.FromDays(180) < DateTime.Now)
        {
            _who5Index = 2;
        }
        else if (SettingsHolder.who5_3months == -1 && SettingsHolder.startDate + TimeSpan.FromDays(90) < DateTime.Now)
        {
            _who5Index = 1;
        }
        else if (SettingsHolder.who5_0months == -1)
        {
            _who5Index = 0;
        }
    }

    public void NextButtonClicked()
    {
        _enqueteController.StartEnquete(_who5Index);
        _mainMenuController.WHO5Flag = false;
        gameObject.SetActive(false);
    }

    public void NeverClicked()
    {
        StartCoroutine(SaveWHO5ToDB());
    }

    private IEnumerator SaveWHO5ToDB()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updateWHO5.php");
        tRequest.SetRequestHeader("WHOSCORE", "-2");
        tRequest.SetRequestHeader("WHOINDEX", _who5Index.ToString());
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
        Debug.Log($"error = {tRequest.error}, text = {tRequest.downloadHandler.text}");
        _mainMenuController.WHO5Flag = false;
        switch (_who5Index)
        {
            case 0:
                SettingsHolder.who5_0months = -2;
                break;
            case 1:
                SettingsHolder.who5_3months = -2;
                break;
            case 2:
                SettingsHolder.who5_6months = -2;
                break;
            case 3:
                SettingsHolder.who5_9months = -2;
                break;
        }

        gameObject.SetActive(false);
    }
}
