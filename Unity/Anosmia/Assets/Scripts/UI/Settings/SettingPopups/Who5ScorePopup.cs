﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Who5ScorePopup : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _scoreText;
    private void OnEnable()
    {
        if (SettingsHolder.who5_9months >=0)
        {
            _scoreText.text = SettingsHolder.who5_9months.ToString();
        }
        else if (SettingsHolder.who5_6months >=0)
        {
            _scoreText.text = SettingsHolder.who5_6months.ToString();
        }
        else if (SettingsHolder.who5_3months >= 0)
        {
            _scoreText.text = SettingsHolder.who5_3months.ToString();
        }
        else if (SettingsHolder.who5_0months >= 0)
        {
            _scoreText.text = SettingsHolder.who5_0months.ToString();
        }
        else
        {
            _scoreText.text = "-";
        }
    }
}
