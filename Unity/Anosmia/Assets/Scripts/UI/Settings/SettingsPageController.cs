﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Settings = SettingsHolder; //creating an alias here

public class SettingsPageController : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _usernameText;
    [SerializeField]
    TextMeshProUGUI _birthYearText;
    [SerializeField]
    TextMeshProUGUI _genderText;
    [SerializeField]
    TextMeshProUGUI _causeOfAnosmiaText;
    [SerializeField]
    TextMeshProUGUI _anosmiaSinceText;
    [SerializeField]
    TextMeshProUGUI _smellTrainingExperienceText;
    [SerializeField]
    TextMeshProUGUI _descripionLessenedSmellText;
    [SerializeField]
    TextMeshProUGUI _personalMotivation;
    [SerializeField]
    GameObject _upsit0Months;
    [SerializeField]
    GameObject _upsit3Months;
    [SerializeField]
    GameObject _upsit6Months;
    [SerializeField]
    GameObject _upsit9Months;
    [SerializeField]
    GameObject _smellSelectionTextSet0;
    [SerializeField]
    GameObject _smellSelectionTextSet1;
    [SerializeField]
    GameObject _smellSelectionTextSet2;
    //[SerializeField]
    //Slider _soundsToggle;

    

    [SerializeField]
    TextMeshProUGUI _notificationsText;
    [SerializeField]
    Slider _notificationsToggle;
    [SerializeField]
    Slider _permissionToUseDataToggle;
    [SerializeField]
    AudioListener audioListener;
    [SerializeField]
    MyNotificationManager notificationsController;


    private void OnEnable()
    {
        _usernameText.text = SocialController.GetUsername;
        UpdateBirthYearText();
        UpdateGenderText();
        UpdateCauseOfAnosmiaText();
        UpdateAnosmiaSinceText();
        UpdateSmellTrainingExperienceText();
        UpdateDescriptionLessenedSmell();
        UpdatePersonalMotivationText();
        UpdateUpsitButtons();
        HandleSmellSet();
        UpdateNotificationText();
        //_soundsToggle.value = PlayerPrefs.HasKey("playSound") ? PlayerPrefs.GetInt("playSound") : 1;
        _notificationsToggle.value = NotificationPlayerPrefHelper.HasEnabledKey ? (NotificationPlayerPrefHelper.NotificationsEnabled ? 1 : 0) : 1;
        _permissionToUseDataToggle.value = Settings.permission ? 1 : 0;
    }

    public void UpdateNotificationText()
    {
        string tMorningTime = NotificationPlayerPrefHelper.HasMorningKey ? (!string.IsNullOrEmpty(NotificationPlayerPrefHelper.MorningTime) ? NotificationPlayerPrefHelper.MorningTime : "06:00") : "06:00";
        string tEveningTime = NotificationPlayerPrefHelper.HasEveningKey ? (!string.IsNullOrEmpty(NotificationPlayerPrefHelper.EveningTime) ? NotificationPlayerPrefHelper.EveningTime : "19:00") : "19:00";
        _notificationsText.text = $"om {tMorningTime} en {tEveningTime}";
    }

    public void UpdayeNotificationSlider()
    {
        _notificationsToggle.value = NotificationPlayerPrefHelper.NotificationsEnabled ? 1 : 0;
    }

    public void ToggleNotifications(float sliderValue)
    {
        notificationsController.CancelAllNotifications();
        if (sliderValue !=0)
        {
            notificationsController.ScheduleNotifications();
        }
        NotificationPlayerPrefHelper.NotificationsEnabled = sliderValue != 0;
    }

    /// <summary>
    /// enables an upsit button if it's supposed to be there
    /// </summary>
    public void UpdateUpsitButtons()
    {
        _upsit0Months.SetActive(Settings.upsit0Months == -1);
        _upsit3Months.SetActive(Settings.upsit3Months == -1 && (DateTime.Now - Settings.startDate).Days > 90);
        _upsit6Months.SetActive(Settings.upsit6Months == -1 && (DateTime.Now - Settings.startDate).Days > 180);
        _upsit9Months.SetActive(Settings.upsit9Months == -1 && (DateTime.Now - Settings.startDate).Days > 270);
    }

    public void UpdatePersonalMotivationText()
    {
        if (PlayerPrefs.HasKey("personalMotivation"))
        {
            _personalMotivation.text = PlayerPrefs.GetString("personalMotivation");
        }
    }

    public void UpdateDescriptionLessenedSmell()
    {
        _descripionLessenedSmellText.text = DiscriptionLessenedSmellDBStringToGameString(Settings.smellAbility);
    }

    public void UpdateSmellTrainingExperienceText()
    {
        _smellTrainingExperienceText.text = SmellTrainingIndexToGameString(Settings.anosmiaTrainingExperience);
    }

    public void UpdateAnosmiaSinceText()
    {
        _anosmiaSinceText.text = Settings.anosmiaFrom == default ? "Ik weet het niet" : Settings.anosmiaFrom.ToString("MMM yyyy", new CultureInfo("nl-NL"));
    }

    public void UpdateCauseOfAnosmiaText()
    {
        _causeOfAnosmiaText.text = CauseOFAnosmiaDBStringToGameString(Settings.causeOfAnosmia);
    }

    public void UpdateGenderText()
    {
        _genderText.text = Settings.gender == 0 ? "-" : Settings.gender == 1 ? "Man" : Settings.gender == 2 ? "Vrouw" : "Anders";
    }

    public void UpdateBirthYearText()
    {
        _birthYearText.text = Settings.birthYear == -2 ? "-" : Settings.birthYear.ToString();
    }

    public void HandleSmellSet()
    {
        _smellSelectionTextSet0.SetActive(Settings.smellSetSelected == 0);
        _smellSelectionTextSet1.SetActive(Settings.smellSetSelected == 1);
        _smellSelectionTextSet2.SetActive(Settings.smellSetSelected == 2);
    }

    private string CauseOFAnosmiaDBStringToGameString(string dbString)
    {
        string[] ints = dbString.Split(',');
        string tResult = "";
        for (int i = 0; i < ints.Length; i++)
        {
            tResult += IntToAnosmiaCause(int.Parse(ints[i]));
            tResult += ", ";
        }
        return tResult.TrimEnd(new char[] {',', ' ' });
    }

    private string IntToAnosmiaCause(int index)
    {
        switch (index)
        {
            case 0: return "oude/bejaarde leeftijd";
            case 1: return "chronische holteontsteking";
            case 2: return "chronische holteontsteking met neuspoliepen";
            case 3: return "allergische reactie zoals hooikoorts";
            case 4: return "virale ziekte zoals een verkoudheid";
            case 5: return "hersenletsel of een beschadiging van de reukzenuwen";
            case 6: return "alzheimer";
            case 7: return "parkinson";
            case 8: return "medicatie";
            case 9: return "chemotherapie";
            case 10: return "blootstelling aan chamische stoffen";
            case 11: return "verontreinigde luchtkwaliteit";
            case 12: return "aangeboren";
            default: return "ik weet het niet";
        }
    }

    private string SmellTrainingIndexToGameString(int index)
    {
        switch (index)
        {
            case 0: return "geen ervaring";
            case 1: return "een paar dagen";
            case 2: return "korter als 3 maanden";
            case 3: return "minimaal 6 maanden";
            case 4: return "langer als 9 maanden";
            default: return "geen ervaring";
        }
    }
    private string DiscriptionLessenedSmellDBStringToGameString(int index)
    {
        switch (index)
        {
            case 0: return "complete anosmie";
            case 1: return "hyposmie ernstig";
            case 2: return "hyposmie gemiddeld";
            case 3: return "hyposmie licht";
            case 4: return "parosmie";
            case 5: return "fantosmie";
            default: return "geen reukstoornis";
        }
    }

    public void ToggleSounds(float value)
    {
        PlayerPrefs.SetInt("playSound", (int)value);
        audioListener.enabled = value != 0;
    }

    public void TogglePermissionToUseData(float value)
    {
        Settings.permission = value != 0;
        StartCoroutine(UpdatePermissionInDB());
    }

    IEnumerator UpdatePermissionInDB()
    {
        UnityWebRequest tRequest = UnityWebRequest.Get(DatabaseHelper.baseUrl + "updatePermission.php");
        tRequest.SetRequestHeader("PERMISSION", Settings.permission ? "1" : "0");
        tRequest.SetRequestHeader("UNIQUEID", SocialController.GetUniqueID);
        yield return tRequest.SendWebRequest();
    }


 
}
