﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleMuter : MonoBehaviour
{
    [SerializeField]
    Toggle[] _toggles;



    public void Mute()
    {
        foreach (var item in _toggles)
        {
            ActualMute(item.onValueChanged);
        }
    }

    public void UnMute()
    {
        foreach (var item in _toggles)
        {
            ActualUnMute(item.onValueChanged);
        }
    }

    private void ActualMute(UnityEngine.Events.UnityEventBase ev)
    {
        int count = ev.GetPersistentEventCount();
        for (int i = 0; i < count; i++)
        {
            ev.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.Off);
        }
    }

    private void ActualUnMute(UnityEngine.Events.UnityEventBase ev)
    {
        int count = ev.GetPersistentEventCount();
        for (int i = 0; i < count; i++)
        {
            ev.SetPersistentListenerState(i, UnityEngine.Events.UnityEventCallState.RuntimeOnly);
        }
    }
}
