﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSlider : MonoBehaviour
{
    [SerializeField]
    Slider _sliderToChange;
    [SerializeField]
    Toggle _myToggle;

    public void ToggleMe(bool value)
    {
        if (value)
        {
            _sliderToChange.value = 1;
        }
        else
        {
            _sliderToChange.value = 0;
        }
    }

    public void ToggleMe(float value)
    {
        _myToggle.isOn = value != 0;
    }
}
